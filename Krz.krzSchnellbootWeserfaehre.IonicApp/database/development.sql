CREATE SCHEMA kallego;

CREATE TABLE kallego.person (
  id INT IDENTITY(1,1) PRIMARY KEY,
  username VARCHAR(255) NOT NULL,
  password NVARCHAR(40) NOT NULL,
  password_hash BINARY(64) NOT NULL,
  last_login DATETIME,
  user_type VARCHAR(50),
  created_at DATETIME,
  updated_at DATETIME
);

CREATE TABLE kallego.ferry(
      id INT IDENTITY(1,1) PRIMARY KEY,
      name VARCHAR(255) NOT NULL,
      last_known_lat DECIMAL(8,6),
      last_known_lon DECIMAL(9,6),
      user_type VARCHAR(50),
      created_at DATETIME,
      updated_at DATETIME,
);

CREATE TABLE kallego.schedule_and_comments(
      id INT IDENTITY(1,1) PRIMARY KEY,
      FERRY_ID_FK INT FOREIGN KEY REFERENCES kallego.ferry(id),
      year DATE,
      schedule NVARCHAR(max),
      created_at DATETIME,
      updated_at DATETIME
);

CREATE TABLE kallego.weather(
      id INT IDENTITY(1,1) PRIMARY KEY,
      FERRY_ID_FK INT FOREIGN KEY REFERENCES kallego.ferry(id),
      temp_high int,
      temp_low int,
      temp_current int,
      weather_type VARCHAR(50),
      wind VARCHAR(50),
      rain VARCHAR(50),
      pegelstand VARCHAR(50),
      created_at DATETIME,
      updated_at DATETIME
);
