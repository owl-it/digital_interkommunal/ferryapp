# Project Kallego Ionic App

## Introduction

This app was built for use by Kalletal and Lemgo as a Ferry Information
Management System, built with the help of System Vertrieb Alexander.

## Layout and HTML

Using the [IONIC](https://ionicframework.com/docs/components) framework the code is contained  
in src/app with the main.ts and app-routing.module.ts to allow for navigation.

## SCSS and Typescript Compilation/Changes

After cloning the project  
```
cd Krz.krzSchnellbootWeserfaehre.IonicApp
npm install
npm run start
```
Open your web browser to: localhost:4200 and shrink your screen to mobile view.

## Sensor Data and where things come from.

Sensor data is from: "http://ferrysensors.azurewebsites.net"  
Weather data is from OpenWeatherMap. 
Schedule Data is from: "https://ferryworkitems.azurewebsites.net/api"

## Login Information for Admin

```
{
	"email": "d.bahl@krz.de",
	"password": "123QWEasd"
}
```

