import { Component, OnInit, Input } from '@angular/core';
import { PositionSensor } from 'src/app/models/position-sensor';

@Component({
  selector: 'app-position',
  templateUrl: './position.component.html',
  styleUrls: ['./position.component.scss'],
})
export class PositionComponent implements OnInit {
  @Input() position: PositionSensor;

  constructor() { }

  ngOnInit() {}

}
