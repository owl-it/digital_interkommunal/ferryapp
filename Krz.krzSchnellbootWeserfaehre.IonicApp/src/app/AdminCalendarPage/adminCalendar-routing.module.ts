import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminCalendarPage } from './adminCalendar.page';

const routes: Routes = [
  {
    path: '',
    component: AdminCalendarPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminCalendarRoutingModule {}
