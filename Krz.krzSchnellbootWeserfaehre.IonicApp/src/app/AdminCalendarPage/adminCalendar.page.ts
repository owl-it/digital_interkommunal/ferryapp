import {Component, OnInit, ViewChild, DoCheck} from '@angular/core';
import {NgForm} from '@angular/forms';
import axios from 'axios';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import { IonModal, IonPopover } from '@ionic/angular';
import { Options } from '../models/NotInServiceOptions.model';
import { Observable, Subject, throwError } from 'rxjs';
import { tap, map, first, catchError } from 'rxjs/operators';
import { FerrymenService } from '../services/ferrymen.service';
import { Users } from '../models/users.model';
import { AccountService } from '../services/accounts.service';
import { ToastController } from '@ionic/angular';
import { Toaster } from '../helpers/toaster';
import { FerryOptions } from '../models/ferryOptions.model';
import { AppSettings } from '../models/appSettings.model';
import { StorageService } from '../services/appSettings.service';

@Component({
  selector: 'app-admin-calendar',
  templateUrl: 'adminCalendar.page.html',
  styleUrls: ['adminCalendar.page.scss']
})
export class AdminCalendarPage implements OnInit{
  @ViewChild(IonModal) modal: IonModal;
  @ViewChild(IonPopover) popover: IonPopover;
  openEndDate: boolean;
  workDate: any;
  startTime: any;
  endtime: any;
  comment: any;
  now: any;
  yesterday: any;
  user: any;
  loggedInUser: any;
  id: any;
  loading: boolean;
  isOperating: boolean;
  showDate: any;
  formSubmit: boolean;
  name: string;
  isModalOpen: boolean;
  isPopoverOpen: boolean;
  freeText: string;
  worker: string;
  results: Observable<any>;
  users: Users[];
  userId: string;
  jwtToken: string;
  toast: Toaster;
  timeStart: any;
  timeEnd: any;

  //options: Options = new Options();
  options = [
    { id: 1, shorttext: 'Hochwasser', text: 'Aufgrund von Hochwasser ist heute leider kein Fährbetrieb möglich.' },
    { id: 2, shorttext: 'Niedrigwasser', text: 'Aufgrund des niedrigen Wasserstandes ist heute leider kein Fährbetrieb möglich.' },
    { id: 3, shorttext: 'Starkregen', text: 'Aufgrund von Starkregen ist derzeit leider kein Fährbetrieb möglich.' },
    { id: 4, shorttext: 'Gewitter', text: 'Aufgrund von Gewitter ist derzeit leider kein Fährbetrieb möglich.' },
    { id: 5, shorttext: 'Sturmböen', text: 'Aufgrund von Sturmböen ist derzeit leider kein Fährbetrieb möglich.' },
    { id: 6, shorttext: 'Starke Winde', text: 'Aufgrund starker Winde ist eine Überfahrt mit der Fähre momentan leider nicht möglich.' },
    { id: 7, shorttext: 'Technischer Defekt',
      text: 'Aufgrund eines technischen Defekts an der Fähre, ist eine Überfahrt mit der Fähre aktuell leider nicht möglich.' },
    { id: 8, shorttext: 'Reparaturarbeiten', text: 'Aufgrund von Reparaturarbeiten derzeit leider kein Fährbetrieb.' },
    { id: 9, shorttext: 'Wartungsarbeiten', text: 'Aufgrund von Wartungsarbeiten heute leider kein Fährbetrieb.' },
    { id: 10, shorttext: 'Personalausfall', text: 'Aufgrund kurzfristigen Personalausfalls findet heute leider kein Fährbetrieb statt.' }
  ];

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private storage: StorageService,
              private ferrymenService: FerrymenService,
              private accountService: AccountService,
              private toastController: ToastController) {
    this.toast = new Toaster(toastController);
    //this.settings = new AppSettings();
    //this.ferrymenService.getOptionData();
    this.getUserInformation();
    this.now = new Date();
    this.yesterday = this.formatDate(this.now.setDate(this.now.getDate() - 1));
    this.openEndDate = false;
    this.workDate = this.formatDate(this.now);
    this.startTime = this.formatDate(this.now);
    this.endtime = this.formatDate(this.now);
    this.comment = '';
    this.user = null;
    this.loggedInUser = null;
    this.loading = false;
    this.isOperating = false;
    this.showDate = null;
    this.formSubmit = false;
    this.isModalOpen = false;
    this.isPopoverOpen = false;
    this.freeText = '';
    this.worker = '';
    this.userId = '';
    this.jwtToken = '';
    this.timeEnd = '';
    this.timeStart = '';
  }

  async getUserInformation() {
    this.jwtToken = this.accountService.accountValue.jwtToken;
    if(this.jwtToken.length === 0) {
         this.router.navigate(['/login']);
    }
    if(this.activatedRoute.snapshot.paramMap.get('id')) {
      this.id = this.activatedRoute.snapshot.paramMap.get('id');
      this.getFahrPlanInformation();
    }
  }

  getDayBeginningTime(date) {
    date = new Date(date);
    const opDate = new Date(this.timeStart);
    // const endString = endTime.getFullYear() + '-' + (endTime.getMonth() + 1) + '-' +
    //   endTime.getDate() + ' '+ '19:00';
    // console.log(endString);
    let string1 = [
      date.getFullYear(),
      this.padTo2Digits(date.getMonth() + 1),
      this.padTo2Digits(date.getDate()),
    ].join('-');
    string1 = string1 + 'T' +
              this.padTo2Digits(opDate.getHours()) + ':' +
              this.padTo2Digits(opDate.getMinutes()) + ':00';
    //console.log(string1);
    return string1;
  }

  getHourEndTime(date) {
    date = new Date(date);
    const opDate = new Date(this.timeEnd);
    // const endString = endTime.getFullYear() + '-' + (endTime.getMonth() + 1) + '-' +
    //   endTime.getDate() + ' '+ '19:00';
    // console.log(endString);
    let string1 = [
      date.getFullYear(),
      this.padTo2Digits(date.getMonth() + 1),
      this.padTo2Digits(date.getDate()),
    ].join('-');
    string1 = string1 + 'T' +
              this.padTo2Digits(opDate.getHours()) + ':' +
              this.padTo2Digits(opDate.getMinutes()) + ':00';
    //console.log(string1);
    return string1;
  }

  padTo2Digits(num) {
    return num.toString().padStart(2, '0');
  }

  formatDate(date) {
    date = new Date(date);

    let dateString = [
      date.getFullYear(),
      this.padTo2Digits(date.getMonth() + 1),
      this.padTo2Digits(date.getDate()),
    ].join('-');
    dateString = dateString + 'T' +
    this.padTo2Digits(date.getHours()) + ':' +
    this.padTo2Digits(date.getMinutes()) + ':00';

    return dateString;
  }

  counter(i: number) {
    return new Array(i);
  }

  dateChanged(datetime) {
    this.workDate = datetime;
    const dtDate = new Date(datetime);
    // set Date to show in the calender element.
    this.showDate = this.padTo2Digits(dtDate.getDate()) + '.' +  this.padTo2Digits(dtDate.getMonth() + 1) + '.' + dtDate.getFullYear();
  }

  startChanged(datetime) {
    this.startTime = datetime;
  }

  endChanged(datetime) {
    this.endtime = datetime;
  }

  endBeforeExpected(datetime) {
    // if the endtime is before the expected endtime the comment field must be filled out.
    const dtDate = new Date(datetime);
    const opDate = new Date(this.timeEnd);
    const dtCompareDate = new Date(dtDate.getFullYear(), dtDate.getMonth(), dtDate.getDate(), opDate.getHours(), opDate.getMinutes(), 0, 0);
    if(dtDate < dtCompareDate)
    {
      return true;
    }
    return false;
  }

  startLaterThanExpected(datetime) {
    // if the endtime is before the expected endtime the comment field must be filled out.
    const dtDate = new Date(datetime);
    const opDate = new Date(this.timeStart);
    if(dtDate.getHours() > opDate.getHours())
    {
      return true;
    }
    return false;
  }

  getDateString(datetime) {
    const dt = new Date(datetime);
    const dtString = this.formatDate(this.workDate) + 'T' +
                     this.padTo2Digits(dt.getHours()) + ':' +
                     this.padTo2Digits(dt.getMinutes()) + ':' +
                     this.padTo2Digits(dt.getSeconds()) + '.' +
                     '000';
    //2022-06-15T16:00:00.000'
    return dtString;
  }

  async onSubmit(form: NgForm) {
    const config = {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      headers: { Authorization: `Bearer ${this.jwtToken}` }
    };
    const dtDate = new Date(this.workDate);
    const dtStartDate = new Date(this.startTime);
    const dtEndDate = new Date(this.endtime);
    this.formSubmit = true;
    // check if there is a comment when the endtime is before the expected endtime.
    if((this.endBeforeExpected(this.endtime) || this.startLaterThanExpected(this.startTime) || !this.isOperating) &&
        this.comment.length < 1)
    {
      return false;
    }
    this.loading= true;
    if(this.id) {
      await axios.put(`https://ferryworkitems.azurewebsites.net/api/ferryworkitem/${this.id}`, {
        workDate: this.workDate,
        start: this.getDateString(dtStartDate),
        end: this.getDateString(dtEndDate),
        comment: this.comment,
        isOperating: this.isOperating,
        userId: this.userId,
        user: this.user
      }, config)
        .then((response) => {
          this.router.navigateByUrl(`/admin`, { replaceUrl: true});
          this.loading= false;
        }).catch((err) => {
        console.log(err);
        this.toast.presentToast('Fehler beim Update des Datensatzes aufgetreten!', 'error');
        this.loading= false;
      });
    } else {
      await axios.post(`https://ferryworkitems.azurewebsites.net/api/ferryworkitem`, {
        //workDate: this.now,
        workDate: this.workDate,
        start: this.getDateString(dtStartDate),
        end: this.getDateString(dtEndDate),
        comment: this.comment,
        isOperating: this.isOperating,
        userId: this.userId,
        user: this.user
      }, config)
        .then((response) => {
          this.router.navigateByUrl(`/admin`, { replaceUrl: true});
          this.loading= false;
        }).catch((err) => {
          console.log(err);
          this.toast.presentToast('Fehler beim speichern des Datensatzes aufgetreten!', 'error');
          this.loading= false;
      });
    }

  }

  getFahrPlanInformation() {
    const config = {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      headers: { Authorization: `Bearer ${this.jwtToken}` }
    };
    axios.get(`https://ferryworkitems.azurewebsites.net/api/ferryworkitem/${this.id}`, config)
      .then(async (response) => {
        if(response.status !== 200) {
          //Generate Error Message
          console.log(response.status + ': Fahrplaninformationen konnten nicht geladen werden!');
          this.toast.presentToast('Fahrplaninformationen konnten nicht geladen werden!', 'error');
          return;
        } else {
          console.log(response.data);
          const { start, end, comment, isOperating, user} = response.data;
          this.startTime = start;
          this.workDate = start;
          this.endtime = end;
          this.comment = comment;
          this.isOperating = isOperating;
          this.user = user;
          this.worker = user.vorname + ' ' + user.nachname;
        }
      });
  }

  getUserInfo() {
    this.results = this.ferrymenService.getUserData();

    this.results.pipe(
      map(res => {
        this.users = res;
      })
    ).subscribe();
  }

  // getOptionData() {
  //   this.results = this.ferrymenService.getOptionData();

  //   this.results.pipe(
  //     catchError(err => {
  //       console.log('Could not load Option Data', err);
  //       this.toast.presentToast('Optionsdaten konnten nicht geladen werden!', 'error');
  //       return throwError(new Error('Optionsdaten konnten nicht geladen werden!'));
  //   }))
  //   .subscribe(res => {
  //     this.settings = res;
  //     this.timeEnd = this.settings.operatingEndTime;
  //     this.timeStart = this.settings.operatingStartTime;
  //   });
  // }

  ngOnInit(): void {
    //this.appSettingService.getConfig().subscribe(settings => { this.settings = settings;});
    this.jwtToken = this.accountService.accountValue.jwtToken;
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.storage.get('operatingStartTime').then((val) => {
      this.timeStart = val;
      this.startTime = this.getDayBeginningTime(this.timeStart);
      this.workDate = this.getDayBeginningTime(this.now);
    });
    this.storage.get('operatingEndTime').then((val) => {
      this.timeEnd = val;
      this.endtime = this.getHourEndTime(this.timeEnd);
    });
  }

  setOpen(isOpen: boolean) {
    this.isModalOpen = isOpen;
    if(!isOpen) {
      this.cancel();
    }
  }

  setPopoverOpen(isOpen: boolean) {
    this.isPopoverOpen = isOpen;
    this.popover.dismiss();
  }

  selectOption(text: string) {
    this.comment = text;
    this.setOpen(false);
  }

  selectUser(user: Users) {
    this.user = user;
    this.worker = user.vorname + ' ' + user.nachname;
    this.userId = user.id;
    this.setPopoverOpen(false);
  }

  cancel() {
    this.modal.dismiss(null, 'cancel');
  }

  confirm() {
    this.modal.dismiss(this.name, 'confirm');
  }
}
