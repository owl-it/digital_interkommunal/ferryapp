import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AdminCalendarPage } from './adminCalendar.page';

import { AdminCalendarRoutingModule } from './adminCalendar-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: AdminCalendarPage }]),
    AdminCalendarRoutingModule,
  ],
  declarations: [AdminCalendarPage]
})
export class AdminCalendarModule {}
