import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './LoginComponent/login.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'forgot',
    loadChildren: () => import('./ForgotPasswordPage/forgot.module').then(m => m.ForgotModule)
  },
  {
    path: 'admin',
    loadChildren: () => import('./AdminPage/admin.module').then(m => m.AdminModule)
  },
  {
    path: '',
    loadChildren: () => import('./FerryPage/ferry.module').then(m => m.FerryModule)
  },
  {
    path: 'admincal',
    loadChildren: () => import('./AdminCalendarPage/adminCalendar.module').then(m => m.AdminCalendarModule)
  },
  {
    path: 'admincal/:id',
    loadChildren: () => import('./AdminCalendarPage/adminCalendar.module').then(m => m.AdminCalendarModule)
  },
  {
    path: 'admin-root',
    loadChildren: () => import('./adminRootPage/adminRoot.module').then( m => m.AdminRootPageModule)
  },
  {
    path: 'mapcomponent',
    loadChildren: () => import('./MapComponent/mapcomponent.module').then( m => m.MapcomponentPageModule)
  },
  {
    path: 'admin-passengers',
    loadChildren: () => import('./admin-passengers/admin-passengers.module').then( m => m.AdminPassengersPageModule)
  },
  {
    path: 'admin-settings',
    loadChildren: () => import('./admin-settings/admin-settings.module').then( m => m.AdminSettingsPageModule)
  },
  {
    path: 'admin-users',
    loadChildren: () => import('./admin-users/admin-users.module').then( m => m.AdminUsersPageModule)
  },
  {
    path: 'admin-users/:id',
    loadChildren: () => import('./admin-users/admin-users.module').then( m => m.AdminUsersPageModule)
  }

];


@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
