import { Component } from '@angular/core';
import { IonDatetime, PopoverController, MenuController } from '@ionic/angular';
import axios from 'axios';
import { formatDate, registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import { stringify } from 'querystring';
registerLocaleData(localeDe, 'de');
import { Router } from '@angular/router';

@Component({
  selector: 'app-ferry',
  templateUrl: 'ferry.page.html',
  styleUrls: ['ferry.page.scss']
})
export class FerryPage {
  openEndDate: boolean;
  popover: boolean;
  currentPopover: any;
  windSpeed: any;
  windKnots: any;
  windDirection: any;
  pegelLevel: any;
  currentTemp: any;
  highAndLowTemp: any;
  weatherType: any;
  weatherIcon: any;
  ferryInOperation: any;
  operationText: string;
  commentText: string;
  startTime: string;
  endTime: string;
  nextDays: any;
  tsFrom: any;
  tsTo: any;
  servicePlanned: boolean;
  isOperatingDay: boolean;

  constructor(public popoverController: PopoverController, private router: Router, private menu: MenuController) {
    this.getWindInformation();
    this.getPegelInformation();
    this.getWeatherInformation();
    this.getUpcomingPlan();

    this.openEndDate = false;
    this.popover = false;
    this.currentPopover = null;
    this.windSpeed = 0;
    this.windKnots = 0;
    this.windDirection = 0;
    this.pegelLevel = 0;
    this.highAndLowTemp = null;
    this.currentTemp = null;
    this.weatherType = null;
    this.weatherIcon = null;
    this.ferryInOperation = null;
    this.operationText = '';
    this.commentText = '';
    this.startTime = '';
    this.endTime = '';
    this.tsFrom = new Date();
    this.tsTo = new Date();
    this.servicePlanned = false;
    this.isOperatingDay = false;
  }

  isPopoverOpen() {
    //console.log(this.popover);
    return this.popover = !this.popover;
  }

  dismissPopover() {
    this.popover = false;
  }

  counter(i: number) {
    return new Array(i);
  }

  getWindInformation() {
    axios.get('https://ferrysensors.azurewebsites.net/api/wind')
      .then((response) => {
        // eslint-disable-next-line no-underscore-dangle
        this.windSpeed = response.data[0]._3WindAve10;
        // eslint-disable-next-line no-underscore-dangle
        this.windDirection =  'rotate('+ response.data[0]._6DirAve10 + 'deg)' ;
        //this.windDirection = 'rotate('+ response.data.wind.deg + 'deg)';
        // winspeed conversion from m/s to km/h
        // eslint-disable-next-line no-underscore-dangle
        this.windSpeed = Math.round(response.data[0]._3WindAve10 * 3.6);
        // eslint-disable-next-line no-underscore-dangle
        this.windKnots = Math.round(response.data[0]._3WindAve10 * 1.944);
        return response.data;
      });
  }

  getPegelInformation() {
    this.tsTo = Date.now();
    this.tsFrom = (Date.now() - 1800000);
    //console.log('Von ' + new Date(this.tsFrom).toISOString() + ' bis ' + new Date(this.tsTo).toISOString() );
    axios.get('https://www.pegelonline.wsv.de/webservices/rest-api/v2/stations/RINTELN/W/' +
              'measurements.json?start=' + new Date(this.tsFrom).toISOString() + '&end=' + new Date(this.tsTo).toISOString())
      .then((response) => {
        // eslint-disable-next-line no-underscore-dangle
        this.pegelLevel = (response.data[0].value / 100).toPrecision(3) + ' m';
        return response.data;
      });
  }

  getWeatherInformation() {
    axios.get('https://api.openweathermap.org/data/2.5/weather?lat=52.1830717&lon=8.9763315' +
      '&appid=178a55481dfea86110366b9308224094&units=metric&lang=de')
      .then((response) => {
        this.currentTemp = Math.round(response.data.main.temp) + '°';
        this.highAndLowTemp = 'H:' + Math.round(response.data.main.temp_max) + '°       T: ' +
          Math.round(response.data.main.temp_min) + '°';
        this.weatherType = response.data.weather[0].description;
        this.weatherIcon = response.data.weather[0].icon;
        // this.windDirection = 'rotate('+ response.data.wind.deg + 'deg)';
        // this.windSpeed = Math.round(response.data.wind.speed * (18/5));
        // this.windKnots = Math.round(response.data.wind.speed * 1.94);
      });
  }

  async getUpcomingPlan() {
    axios.get(`https://ferryworkitems.azurewebsites.net/api/ferryworkitem/upcoming`)
      .then(async (response) => {
        const upcoming = response.data[0];
        this.nextDays = response.data;
        //console.log(this.nextDays);
        const { start, end, comment, isOperating } = upcoming;
        const startDate = new Date(start);
        const endDate = new Date(end);
        this.startTime = this.padTo2Digits(startDate.getHours()) + ':' + this.padTo2Digits(startDate.getMinutes()) + ' -';
        this.endTime = this.padTo2Digits(endDate.getHours()) + ':' + this.padTo2Digits(endDate.getMinutes());
        const ferryInOperation = await this.determineIfShipIsInService(upcoming);
        if(!ferryInOperation) {
          this.ferryInOperation = false;
          this.operationText = 'Aktuell nicht in Betrieb';
          if(this.isOperatingDay) {
            if(comment.toString().length < 1) {
              this.commentText = 'Nächste Überfahrt siehe Fahrplan';
            } else {
              this.commentText = comment;
            }
          } else {
            this.commentText = 'Nächste Überfahrt siehe Fahrplan';
          }
        } else {
          this.ferryInOperation = true;
          this.operationText = 'Fähre in Betrieb';
          this.commentText = comment;
        }
      })
      .catch(error => {
        //this.operationText = 'Aktuell nicht in Betrieb';
        //this.commentText = 'Die Fähre befindet sich aktuell in Winterpause';
      });
  }

  padTo2Digits(num) {
    return String(num).padStart(2, '0');
  }

  determineIfShipIsInService(upcoming) {
    const { start, end, isOperating, workDate } = upcoming;
    const now = new Date();
    const startDate = new Date(start);
    const endDate = new Date(end);
    this.isOperatingDay = false;
    const workDay = new Date(workDate);
    const today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
    const tday = new Date(workDay.getFullYear(), workDay.getMonth(), workDay.getDate());
    if(today.getTime() === tday.getTime()) {
      this.isOperatingDay = true;
    }
    if(startDate < now && endDate > now) {
      if(isOperating) {
        return true;
      } else {
        return false;
      }
    }
    return false;
  }

  menurouting(target: string) {
    this.router.navigateByUrl('/login');
  }

  menuopen() {
    this.menu.enable(true).then(() => {
      this.menu.open();
    });
  }

  pageRefresh(event) {
    this.getWindInformation();
    this.getPegelInformation();
    this.getWeatherInformation();
    this.getUpcomingPlan();
    event.target.complete();
  }

  async handleButtonClick(ev) {
    const popover = await this.popoverController.create({
      component: 'popover-example-page',
      event: ev,
      translucent: true,
    });
    await popover.present();
    const { role } = await popover.onDidDismiss();
    //console.log('onDidDismiss resolved with role', role);
  }

  ionViewWillEnter() {
    //console.log('FerryPage - ViewWillEnter');
    this.menu.close('login');
    // this.menu.enable(true).then(() => {
    //   this.menu.close();
    // });
  }
  ionViewWillLeave() {
    // This method will be called every time the component is navigated away from
    // It would be a good method to call cleanup code such as unsubscribing from observables

    //console.log('FerryPage - ViewWillLeave');

    this.menu.enable(true).then(() => {
      this.menu.close();
    });
  }
}


