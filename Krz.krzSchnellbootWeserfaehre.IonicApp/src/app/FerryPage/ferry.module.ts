import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FerryPage } from './ferry.page';

import { FerryRoutingModule } from './ferry-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: FerryPage }]),
    FerryRoutingModule,
  ],
  declarations: [FerryPage]
})
export class FerryModule {}
