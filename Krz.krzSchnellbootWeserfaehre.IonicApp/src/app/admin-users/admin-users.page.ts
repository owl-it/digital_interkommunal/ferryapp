import { Component, OnInit } from '@angular/core';
import { AccountService } from '../services/accounts.service';
import { Observable, throwError } from 'rxjs';
import { map, first, catchError } from 'rxjs/operators';
import { AlertController } from '@ionic/angular';
import { Router, ActivatedRoute, ResolveStart } from '@angular/router';
import { Account } from '../models/account.model';
import { Toaster } from '../helpers/toaster';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.page.html',
  styleUrls: ['./admin-users.page.scss'],
})
export class AdminUsersPage implements OnInit {
  account: Account;
  id: string;
  response: any;
  handlerMessage = '';
  roleMessage = '';
  toast: Toaster;

  constructor(
    private accountService: AccountService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private alertController: AlertController,
    private toastController: ToastController) {
      this.toast = new Toaster(toastController);
     }

  ngOnInit() {
    this.account = new Account();
    if(this.activatedRoute.snapshot.paramMap.get('id')) {
      this.id = this.activatedRoute.snapshot.paramMap.get('id');
      this.getUserInfo(this.id);
    }
  }

  getUserInfo(id: string) {
    this.accountService.getById(id)
      .pipe(
      catchError(err => {
        console.log('Could not load User Data', err);
        this.toast.presentToast('Userdaten konnten nicht geladen werden!', 'error');
        return throwError(new Error('Userdaten konnten nicht geladen werden!'));
      }))
      .subscribe(accounts => {
        this.account = accounts;
      });
  }

  saveAccount() {
    if(this.id) {
      this.accountService.update(this.id, this.account)
        .pipe(
          catchError(err => {
            console.log('Could not update Account Data', err);
            this.toast.presentToast('Userdaten konnten nicht geupdatet werden!', 'error');
            return throwError(new Error('Userdaten konnten nicht geupdatet werden!'));
        }))
        .subscribe(accounts => {
          this.response = accounts;
        },
          () => this.toast.presentToast('User erfolgreich aktualisiert!', 'info')
        );
    } else {
      this.accountService.create(this.account)
        .pipe(
          catchError(err => {
            console.log('Could not create Account Data', err);
            this.toast.presentToast('Userdaten konnten nicht erzeugt werden!', 'error');
            return throwError(new Error('Userdaten konnten nicht erzeugt werden!'));
        }))
        .subscribe(accounts => {
          this.response = accounts;
        },
          () => this.toast.presentToast('User erfolgreich angelegt!', 'info')
        );
    }
  }

  deleteUser() {
    //console.log('deleteUser(' + this.account.id, this.account.email + ')');
    this.accountService.delete(this.id)
      .pipe(
        catchError(err => {
          console.log('Could not delete Account Data', err);
          this.toast.presentToast('Userdaten konnten nicht gelöscht werden!', 'error');
          return throwError(new Error('Userdaten konnten nicht gelöscht werden!'));
      }))
      .subscribe(accounts => {
        this.response = accounts;
      },
        () => this.toast.presentToast('User erfolgreich gelöscht!', 'info')
      );
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Benutzeraccount wirklich löschen?',
      subHeader: 'Diese Aktion können Sie nicht mehr rückgängig machen.',
      message: 'Wollen Sie den Account ' + this.account.email + ' endgültig löschen?',
      buttons: [
        {
          text: 'Abbrechen',
          role: 'cancel',
          handler: () => {
            this.handlerMessage = 'delete canceled';
          },
        },
        {
          text: 'OK',
          role: 'confirm',
          handler: () => {
            this.handlerMessage = 'Account wurde gelöscht!';
          },
        },
      ],
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    if(role === 'confirm')
    {
      this.deleteUser();
    };
  }
}
