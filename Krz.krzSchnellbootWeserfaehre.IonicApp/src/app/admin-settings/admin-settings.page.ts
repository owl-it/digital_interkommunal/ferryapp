import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, first } from 'rxjs/operators';
import { FerrymenService } from '../services/ferrymen.service';
import { AccountService } from '../services/accounts.service';
import { IonModal } from '@ionic/angular';
import {Router, ActivatedRoute, NavigationExtras} from '@angular/router';
import { environment } from 'src/environments/environment';
import { FerryOptions, CreateFerryOptions } from '../models/ferryOptions.model';
import { Toaster } from '../helpers/toaster';
import { ToastController } from '@ionic/angular';
import { StorageService } from '../services/appSettings.service';

const baseUrl = `${environment.apiUrl}/accounts`;

@Component({
  selector: 'app-admin-settings',
  templateUrl: './admin-settings.page.html',
  styleUrls: ['./admin-settings.page.scss'],
})
export class AdminSettingsPage implements OnInit {
  @ViewChild(IonModal) modal: IonModal;
  accounts: any[];
  results: any[];
  ferryOptions: FerryOptions[];
  ferryOption: CreateFerryOptions;
  toast: Toaster;
  id: string;
  seasonStartDate: any;
  seasonEndDate: any;
  operatingStartTime: any;
  operatingEndTime: any;
  now: any;

  constructor(
    private ferrymenService: FerrymenService,
    private accountService: AccountService,
    private storage: StorageService,
    private router: Router,
    private route: ActivatedRoute,
    private toastController: ToastController) {
      this.toast = new Toaster(toastController);
      this.now = this.formatDate(new Date());
      this.seasonStartDate = this.now;
      this.seasonEndDate = this.now;
      this.operatingStartTime = this.now;
      this.operatingEndTime = this.now;
      this.ferryOptions = [];
      this.ferryOption = new CreateFerryOptions();
    }

  ngOnInit() {
    this.getUserInfo();
    this.getOptionData();
  }

  padTo2Digits(num) {
    return num.toString().padStart(2, '0');
  }

  formatDate(date) {
    date = new Date(date);

    let dateString = [
      date.getFullYear(),
      this.padTo2Digits(date.getMonth() + 1),
      this.padTo2Digits(date.getDate()),
    ].join('-');
    dateString = dateString
                 + 'T'
                 + this.padTo2Digits(date.getHours())
                 + ':'
                 + this.padTo2Digits(date.getMinutes())
                 + ':'
                 + this.padTo2Digits(date.getSeconds())
                 + '.000Z';
    return dateString;
  }

  getUserInfo() {
    this.accountService.getAll()
      .pipe(first(),
      catchError(err => {
        console.log('Could not load Account Data', err);
        this.toast.presentToast('Userdaten konnten nicht geladen werden!', 'error');
        return throwError(new Error('Userdaten konnten nicht geladen werden!'));
    }))
      .subscribe(accounts => {
        this.accounts = accounts;
      });
  }

  getOptionData() {
    this.storage.get('operatingEndtime').then((val) => this.operatingEndTime = val);
    this.storage.get('operatingStarttime').then((val) => this.operatingStartTime = val);
    this.storage.get('seasonEnddate').then((val) => this.seasonEndDate = val);
    this.storage.get('seasonStartdate').then((val) => this.seasonStartDate = val);
  //   this.ferrymenService.getOptionData()
  //     .pipe(
  //     catchError(err => {
  //       console.log('Could not load Option Data', err);
  //       this.toast.presentToast('Optionsdaten konnten nicht geladen werden!', 'error');
  //       return throwError(new Error('Optionsdaten konnten nicht geladen werden!'));
  //   }))
  //     .subscribe(res => {
  //       this.ferryOptions = res;
  //       if(this.ferryOptions.length > 0) {
  //         this.id = this.ferryOptions[0].id;
  //         this.operatingEndTime = this.formatDate(this.ferryOptions[0].operatingEndTime);
  //         this.operatingStartTime = this.formatDate(this.ferryOptions[0].operatingStartTime);
  //         this.seasonEndDate = this.formatDate(this.ferryOptions[0].seasonEndDate);
  //         this.seasonStartDate = this.formatDate(this.ferryOptions[0].seasonStartDate);
  //       }
  //       console.log('Optiondata: ' + res);
  //   });
  }

  setOptionData() {
    this.ferrymenService.setOptionData(this.id, {
      seasonStartDate: this.formatDate(this.seasonStartDate),
      seasonEndDate: this.formatDate(this.seasonEndDate),
      operatingStartTime: this.formatDate(this.operatingStartTime),
      operatingEndTime: this.formatDate(this.operatingEndTime)
    })
      .pipe(first(),
            catchError(err => {
              console.log('Could not save Option Data', err);
              this.toast.presentToast('Optionsdaten konnten nicht gespeichert werden!', 'error');
              return throwError(new Error('Optionsdaten konnten nicht gespeichert werden!'));
          }))
      .subscribe(res => {
        this.results = res;
        this.storage.set('operatingEndTime', this.operatingEndTime);
        this.storage.set('operatingStartTime', this.operatingStartTime);
        this.storage.set('seasonEndDate', this.seasonEndDate);
        this.storage.set('seasonStartDate', this.seasonStartDate);
      },
        () => this.toast.presentToast('Optionsdaten gespeichert!', 'info')
      );
  }

  createOperatingDays() {
    this.toast.presentToast('Fährtermine der aktuellen Saison erzeugt!', 'info');
  }
  addUser() {
    this.router.navigateByUrl('/admin-users/',  { replaceUrl: true});
  }

  navigateUser(id: string) {
    this.router.navigateByUrl(`/admin-users/${id}`, { replaceUrl: true});
    //this.router.navigateByUrl(`${baseUrl}/${id}`, { replaceUrl: true});
  }
}

