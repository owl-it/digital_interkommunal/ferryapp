import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { stringify } from 'querystring';

@Injectable()
export class Toaster{

    constructor(private toaster: ToastController) {};

    async presentToast(text: string, severity: string) {
        // severity Options:
        // - info - Information Toast
        // - warning
        // - error
        let color: any;
        let toastIcon: string;

        switch(severity) {
            case 'warning':
                color = 'toast-warning';
                toastIcon = 'alert-circle-outline';
                break;

            case 'error':
                color = 'toast-error';
                toastIcon = 'close-circle-outline';
                break;

            default:
                color = 'toast-info';
                toastIcon = 'information-circle-outline';
                break;
        }
        const toast = await this.toaster.create({
          position: 'middle',
          icon: toastIcon,
          cssClass: color,
          message: text,
          duration: 3000,
        });
        await toast.present();
      }
}
