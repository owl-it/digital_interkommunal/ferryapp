import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ForgotPage } from './forgot.page';

import { Tab6PageRoutingModule } from './forgot-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    Tab6PageRoutingModule
  ],
  declarations: [ForgotPage]
})
export class ForgotModule {}
