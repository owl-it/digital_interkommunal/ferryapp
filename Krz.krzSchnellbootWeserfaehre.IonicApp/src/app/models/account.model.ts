export interface IAccount {
    id: string;
    title: string;
    firstName: string;
    lastName: string;
    email: string;
    role: string;
    created: Date;
    updatet: Date;
    isVerified: boolean;
    jwtToken: string;
    password: string;
    confirmPassword: string;
}
export class Account implements IAccount{
    public id: string;
    public title: string;
    public firstName: string;
    public lastName: string;
    public email: string;
    public role: string;
    public created: Date;
    public updatet: Date;
    public isVerified: boolean;
    public jwtToken: string;
    public password: string;
    public confirmPassword: string;
};
