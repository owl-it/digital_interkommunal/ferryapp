export interface IOptions {
    id: number;
    shorttext: string;
    text: string;
}

export class Options implements IOptions {
    public id: number;
    public shorttext: string;
    public text: string;
}
