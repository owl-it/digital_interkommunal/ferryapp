export class Passengers {
    public id: string;
    public pedestrians: number;
    public cyclists: number;
    public wheelchairs: number;
    public ferryWorkItemId: string;
}

export class CreatePassengers {
    public pedestrians: number;
    public cyclists: number;
    public wheelchairs: number;
    public ferryWorkItemId: string;
}
