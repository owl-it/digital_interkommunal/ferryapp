export class PositionSensor {
    public received: string;
    public battery: number;
    public lat: number;
    public lon: number;
    public speed: number;
    public temperature: number;
}
