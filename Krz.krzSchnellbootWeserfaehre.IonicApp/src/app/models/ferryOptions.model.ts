export interface IFerryOptions {
    id: string;
    seasonStartDate: string;
    seasonEndDate: string;
    operatingStartTime: string;
    operatingEndTime: string;
}

export class FerryOptions implements IFerryOptions {
    public id: string;
    public seasonStartDate: string;
    public seasonEndDate: string;
    public operatingStartTime: string;
    public operatingEndTime: string;
}

export interface ICreateFerryOptions {
    seasonStartDate: string;
    seasonEndDate: string;
    operatingStartTime: string;
    operatingEndTime: string;
}

export class CreateFerryOptions implements ICreateFerryOptions {
    public seasonStartDate: string;
    public seasonEndDate: string;
    public operatingStartTime: string;
    public operatingEndTime: string;
}

