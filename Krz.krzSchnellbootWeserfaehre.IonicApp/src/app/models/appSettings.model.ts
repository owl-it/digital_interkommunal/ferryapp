export class AppSettings {
    seasonStartdate: string;
    seasonEnddate: string;
    operatingStarttime: string;
    operatingEndtime: string;
}
