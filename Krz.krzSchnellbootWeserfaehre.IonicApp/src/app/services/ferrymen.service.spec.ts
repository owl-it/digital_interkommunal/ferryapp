import { TestBed } from '@angular/core/testing';

import { FerrymenService } from './ferrymen.service';

describe('FerrymenService', () => {
  let service: FerrymenService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FerrymenService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
