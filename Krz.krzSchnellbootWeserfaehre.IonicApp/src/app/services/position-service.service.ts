import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { PositionSensor } from '../models/position-sensor';

@Injectable({
  providedIn: 'root'
})
export class PositionServiceService {
  response: Observable<any>;
  position: PositionSensor;

  constructor(public http: HttpClient) { }

  public getPositionData(): Observable<PositionSensor> {
    this.response = this.http.get('https://ferrysensors.azurewebsites.net/api/position');

    return this.response;
  }
}
