import { HttpClient } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap, map } from 'rxjs/operators';
//import { Storage } from '@ionic/storage-angular';
import { Router } from '@angular/router';
import { CreatePassengers, Passengers } from '../models/ferryPassengers.model';
import { FerryOptions, CreateFerryOptions } from '../models/ferryOptions.model';
import { AccountService } from './accounts.service';

@Injectable({
  providedIn: 'root'
})
export class FerrymenService{
  response: Observable<any>;
  userInfo: any;
  //storageHolder: Storage;
  url: string;
  jwtToken: string;
  public ferryOption: Observable<FerryOptions>;
  private ferrySubject: BehaviorSubject<FerryOptions>;

  constructor(public http: HttpClient, public router: Router, private accountService: AccountService) {
    this.ferrySubject = new BehaviorSubject<FerryOptions>(null);
    this.ferryOption = this.ferrySubject.asObservable();
    this.userInfo = null;
    //this.storageHolder = storage;
    //this.getUserInformation();
    this.url = 'https://ferryworkitems.azurewebsites.net/api/';
  }

  public get ferryOptions(): FerryOptions {
    return this.ferrySubject.value;
  }

  public getOptionData(): Observable<FerryOptions> {
    this.getUserInformation();
    const config = {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      headers: { Authorization: `Bearer ${this.accountService.accountValue.jwtToken}` }
    };
    return this.http.get<any>(this.url + 'ferryoptions', config)
    .pipe(map(options => {
        this.ferrySubject.next(options);
        return options;
    }));
  }

  public setOptionData(id: string, options: CreateFerryOptions): Observable<any> {
    const config = {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      headers: { Authorization: `Bearer ${this.accountService.accountValue.jwtToken}` }
    };
    if(id) {
      return this.http.put<Passengers>(this.url + `ferryoptions/${id}`, options, config);
    }
    return this.http.post<Passengers>(this.url + 'ferryoptions', options, config)
    .pipe(
      //console.log("Error saving PassengerCount.");
    );
  }

  public deleteOptionData(id: string): Observable<any> {
    return this.http.delete(this.url + `ferryoptions/${id}`);
  }

  public getUserData(): Observable<any> {
    const config = {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      headers: { Authorization: `Bearer ${this.accountService.accountValue.jwtToken}` }
    };
    this.response = this.http.get(this.url + 'usersplain', config);
    return this.response;
  }

  public getSingleUserData(userId): Observable<any> {
    const config = {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      headers: { Authorization: `Bearer ${this.accountService.accountValue.jwtToken}` }
    };
    this.response = this.http.get(this.url + `user/${userId}`, config);
    return this.response;
  }

  public setPassengerCount(passengers: Passengers): Observable<any> {
    const config = {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      headers: { Authorization: `Bearer ${this.accountService.accountValue.jwtToken}` }
    };
    if(passengers.id) {
      return this.http.put<Passengers>(this.url + `ferrypassengers/${passengers.id}`, passengers, config);
    }
    return this.http.post<Passengers>(this.url + 'ferrypassengers', passengers, config)
    .pipe(
      //console.log("Error saving PassengerCount.");
    );
  }

  public getPassengerCount(ferryWorkItemId): Observable<any> {
    const config = {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      headers: { Authorization: `Bearer ${this.accountService.accountValue.jwtToken}` }
    };
    this.response = this.http.get(this.url + `ferrypassengers/ferrypassengersbyitem/${ferryWorkItemId}`, config);
    return this.response;
  }

  // AccountService?
  public getRegisteredUsers(): Observable<any> {
    const config = {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      headers: { Authorization: `Bearer ${this.accountService.accountValue.jwtToken}` }
    };
    this.response = this.http.get('https://web-app-iot-smart-app-weserfaehre.azurewebsites.net/accounts', config);
    return this.response;
  }

  async getUserInformation() {
    // eslint-disable-next-line no-underscore-dangle
    // try {
    //   return new Promise(resolve => {
    //     this.storageHolder.get('user').then((data) => {
    //       if(data) {
    //         this.userInfo = data;
    //       }
    //     });
    //   });
    // } catch(e) {
    //   console.log(e);
    //   this.router.navigate(['/login']);
    // }
    this.jwtToken = this.accountService.accountValue.jwtToken;
  }
}
