import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router, ActivatedRoute, NavigationExtras} from '@angular/router';
import { StorageService } from '../services/appSettings.service';
import axios from 'axios';
//import { FerrymenService } from '../services/ferrymen.service';
import { AccountService } from '../services/accounts.service';
import { ToastController } from '@ionic/angular';
import { Toaster } from '../helpers/toaster';

@Component({
  selector: 'app-admin',
  templateUrl: 'admin.page.html',
  styleUrls: ['admin.page.scss']
})
export class AdminPage implements OnInit, OnDestroy {
  userInfo: any;
  dienstList: Array<any>;
  currentLoading: boolean;
  privateName: UserArray[];
  now: any;
  jwtToken: string;
  toast: Toaster;


  constructor(private router: Router,
              private route: ActivatedRoute,
              public storage: StorageService,
              //private ferrymenService: FerrymenService,
              private accountService: AccountService,
              private toastController: ToastController) {
    this.toast = new Toaster(toastController);
    this.userInfo = null;
    this.dienstList = [];
    this.currentLoading = true;
    this.privateName = [{name: ''}];
    this.now = new Date();
    this.getUserInformation();
  }

  async getUserInformation() {
    // eslint-disable-next-line no-underscore-dangle
    // try {
    //   return new Promise(resolve => {
    //     this.storageHolder.get('user').then((data) => {
    //       if(data) {
    //         this.userInfo = data;
    //         this.getUpcoming();
    //       }
    //     });
    //   });
    // } catch(e) {
    //   this.router.navigate(['/login']);
    // }
    this.jwtToken = this.accountService.accountValue.jwtToken;
    if(this.jwtToken.length > 0) {
      this.getUpcoming();
    }
  }

  getUpcoming() {
    const config = {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      headers: { Authorization: `Bearer ${this.jwtToken}` }
    };
    if(this.jwtToken !== null) {
      axios.get(`https://ferryworkitems.azurewebsites.net/api/ferryworkitem`, config)
        .then(async (response) => {
          this.currentLoading = false;
          if(response.status !== 200) {
            //Generate Error Message
            console.log(response.status + ': Fahrplaninformationen konnten nicht geladen werden!');
            this.toast.presentToast('Fahrplaninformationen konnten nicht geladen werden!', 'error');
            return;
          } else {
            for(const plan of response.data) {
              const newPlanObj = {
                id: plan.id,
                userName: plan.user.vorname + ' ' + plan.user.nachname,
                // workDate: plan.workDate,
                // start: new Date(plan.start).getDay(),
                // end: plan.end,
                // comment: plan.comment,
                // isOperating: plan.isOperating,
                userId: plan.userId,
                dayOfOperation: this.getDayOfWeek(new Date(plan.start).getDay()),
                dateOfOperation: this.formatDate(new Date(plan.start)),
                startOfDay: new Date(plan.start).getHours() + ':' + this.padTo2Digits(new Date(plan.start).getMinutes()),
                endOfDay: new Date(plan.end).getHours() + ':' + this.padTo2Digits(new Date(plan.end).getMinutes()),
              };
              if(new Date(plan.start) >= new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate(), 0, 0, 0)) {
                this.dienstList.push(newPlanObj);
              }
            }
          }
        });
    }
  }

  getDayOfWeek(num) {
    const dayArr = [
      'Sonntag',
      'Montag',
      'Dienstag',
      'Mittwoch',
      'Donnerstag',
      'Freitag',
      'Samstag'
    ];

    return dayArr[num];
  }

  padTo2Digits(num) {
    return num.toString().padStart(2, '0');
  }

  formatDate(date) {
    return [
      this.padTo2Digits(date.getDate()),
      this.padTo2Digits(date.getMonth() + 1),
      date.getFullYear()
    ].join('.');
  }

  addDienstPlan() {
    //If JWT token is valid, refresh?
    this.router.navigateByUrl(`/admincal/`, { replaceUrl: true});
  }

  editPlan(id) {
    this.router.navigateByUrl(`/admincal/${id}`, { replaceUrl: true});
  }

  addPassengers(ferryworkItemId) {
    const navigationExtras: NavigationExtras = {
      state: {
        id: ferryworkItemId
      }
    };
    this.router.navigateByUrl(`/admin-passengers`, navigationExtras);
  }

  deletePlan(id) {
    this.currentLoading = true;
    this.dienstList = [];
    const config = {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      headers: { Authorization: `Bearer ${this.jwtToken}` }
    };
    axios.delete(`https://ferryworkitems.azurewebsites.net/api/ferryworkitem/${id}`, config)
      .then((response) => {
        if(response.status !== 200) {
          this.toast.presentToast('Planelement konnte nicht gelöscht werden!', 'error');
        } else {
        // Refresh the dienstList
        this.getUpcoming();
        this.toast.presentToast('Planelement erfolgreich gelöscht!', 'info');
        }
      });
  }

  // async getFahrPersonInformation() {
  //   const config = {
  //     // eslint-disable-next-line @typescript-eslint/naming-convention
  //     headers: { Authorization: `Bearer ${this.jwtToken}` }
  //   };
  //   if(this.jwtToken !== null) {
  //     await axios.get(`https://ferryworkitems.azurewebsites.net/api/ferryworkitem`, config)
  //       .then(async (response) => {
  //         if(response.status !== 200) {
  //           //Generate Error Message
  //           return;
  //         } else {
  //           // await this.storage.set('user', response.data);
  //           // this.router.navigate(['/admin'], navigationExtras);
  //         }
  //       });
  //   }
  // }

  public names(): UserArray[] {
    return this.privateName;
  }

  updateValue(index: number, value: string) {
    this.privateName[index].name = value;
  }

  onPlusClick() {
    this.privateName.push({name: ''});
  }

  onMinusClick(index: number) {
    this.privateName.splice(index, 1);
  }

  async ngOnInit() {
    // With Routing in Ionic, The OnInit lifecycle hook
    // may not get called consistently.
    //console.log('AdminPage - OnInit');
  }

  ngOnDestroy() {
    // Likewise, this will may not consistently fire when you navigate away
    // from the component
    //console.log('AdminPage - OnDestroy');
  }

  // However, Ionic provides lifecycle hooks of its own that
  // will fire consistently during route navigation

  ionViewWillEnter() {
    // This method will be called every time the component is navigated to
    // On initialization, both ngOnInit and this method will be called
    //console.log('AdminPage - ViewWillEnter');
  }
  ionViewDidEnter() {
    // this.getUserInformation(this.storage);
    console.log('AdminPage - ViewDidEnter');
    // Reload current view.
    //this.getUserInformation();
  }

  ionViewWillLeave() {
    // This method will be called every time the component is navigated away from
    // It would be a good method to call cleanup code such as unsubscribing from observables

    //console.log('AdminPage - ViewWillLeave');
  }
}

export interface UserArray {
  name: string;
}
