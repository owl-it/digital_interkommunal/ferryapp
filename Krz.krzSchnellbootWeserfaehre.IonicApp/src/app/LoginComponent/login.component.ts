import { Component, OnInit, OnDestroy } from '@angular/core';
import {NgForm} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import axios from 'axios';
import { StorageService } from '../services/appSettings.service';
import { MenuController } from '@ionic/angular';
import { AccountService } from '../services/accounts.service';
import { first, map } from 'rxjs/operators';
import { FerrymenService } from '../services/ferrymen.service';
import { FerryOptions } from '../models/ferryOptions.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit, OnDestroy {
  username: string;
  password: string;
  formSubmit: boolean;
  errorMessage: any;
  loading: boolean;
  ferryOptions: FerryOptions;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private menu: MenuController,
    private accountService: AccountService,
    private storage: StorageService,
    private ferrymenService: FerrymenService) {
    this.username = '';
    this.password = '';
    this.formSubmit = false;
    this.loading = false;
    this.errorMessage = '';
    this.ferryOptions = new FerryOptions();
  }

appInitializer(accountService: AccountService) {
    return () => new Promise(resolve => {
        // attempt to refresh token on app start up to auto authenticate
        accountService.refreshToken()
            .subscribe()
            .add(resolve);
    });
  }

  onSubmit(form: NgForm) {
    this.formSubmit = true;
    this.loading = true;
    this.accountService.login(this.username, this.password)
    .pipe(first())
    .subscribe({
        next: () => {
            // get return url from query parameters or default to home page
            //const returnUrl = this.route.snapshot.queryParams['/admin-root'] || '/';
            this.appInitializer(this.accountService);
            // initialize local storage
            this.getSystemSettings();
            this.router.navigateByUrl('/admin-root');
        },
        error: error => {
            this.errorMessage.error(error);
            this.loading = false;
        }
    });
    // axios.post('https://web-app-iot-smart-app-weserfaehre.azurewebsites.net/Accounts/authenticate', {
    //   email: this.username,
    //   password: this.password
    // }).then(async (response) => {
    //   this.loading = false;
    //   console.log(response);
    //   if(response.status !== 200) {
    //     //Generate Error Message
    //     this.errorMessage = response;
    //     return;
    //   } else {
    //     console.log(response.data);
    //     // setting the user data to storage -> response is the json with user data from authAPI
    //     await this.storage.set('user', response.data);
    //     this.router.navigateByUrl('/admin-root', {
    //       replaceUrl : true
    //     });
    //   }
    // }).catch((e) => {
    //   this.loading = false;
    //   this.errorMessage = e.response.data.message;
    // });

  }

  getSystemSettings() {
    this.ferrymenService.getOptionData()
      .pipe(map( settings => {
        this.ferryOptions = settings;
        this.storage.set('operatingEndTime', this.ferryOptions[0].operatingEndTime);
        this.storage.set('operatingStartTime', this.ferryOptions[0].operatingStartTime);
        this.storage.set('seasonEndDate', this.ferryOptions[0].seasonEndDate);
        this.storage.set('seasonStartDate', this.ferryOptions[0].seasonStartDate);
      }))
      .subscribe(
      );
  }

  async ngOnInit() {
    // With Routing in Ionic, The OnInit lifecycle hook
    // may not get called consistently.
    //console.log('LoginPage - OnInit');
    //await this.storage.create();
  }

  menurouting(target: string) {
    this.router.navigateByUrl('/');
  }

  menuopen() {
    this.menu.enable(true).then(() => {
      this.menu.open();
    });
  }

  ngOnDestroy() {
    // Likewise, this will may not consistently fire when you navigate away
    // from the component
    //console.log('LoginPage - OnDestroy');
  }

  // However, Ionic provides lifecycle hooks of its own that
  // will fire consistently during route navigation

  ionViewWillEnter() {
    // This method will be called every time the component is navigated to
    // On initialization, both ngOnInit and this method will be called

    //console.log('LoginPage - ViewWillEnter');
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter - Login Page');
  }

  ionViewWillLeave() {
    // This method will be called every time the component is navigated away from
    // It would be a good method to call cleanup code such as unsubscribing from observables

    //console.log('LoginPage - ViewWillLeave');

    this.menu.enable(true).then(() => {
      this.menu.close();
    });
  }
}
