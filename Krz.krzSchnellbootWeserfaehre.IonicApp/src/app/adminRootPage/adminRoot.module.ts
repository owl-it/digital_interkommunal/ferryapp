import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdminRootPageRoutingModule } from './adminRoot-routing.module';

import { AdminRootPage } from './adminRoot.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdminRootPageRoutingModule
  ],
  declarations: [AdminRootPage]
})
export class AdminRootPageModule {}
