import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminRootPage } from './adminRoot.page';

const routes: Routes = [
  {
    path: '',
    component: AdminRootPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRootPageRoutingModule {}
