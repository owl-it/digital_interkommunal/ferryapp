import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { Account } from '../models/account.model';
import { AccountService } from '../services/accounts.service';

@Component({
  selector: 'app-admin-root',
  templateUrl: './adminRoot.page.html',
  styleUrls: ['./adminRoot.page.scss'],
})
export class AdminRootPage implements OnInit {
  isAdmin: boolean;
  //account: Account;
  constructor(private router: Router, private accountService: AccountService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.isAdmin = false;
    if(this.accountService.accountValue.role === 'Admin') {
      this.isAdmin = true;
    }
  }

  menurouting(target: string) {
    this.router.navigateByUrl('/' + target);
  }

}
