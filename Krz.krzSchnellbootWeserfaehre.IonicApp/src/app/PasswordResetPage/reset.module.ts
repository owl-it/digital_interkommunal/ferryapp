import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ResetPage } from './reset.page';
//import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { ResetRoutingModule } from './reset-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    //ExploreContainerComponentModule,
    ResetRoutingModule
  ],
  declarations: [ResetPage]
})
export class ResetModule {}
