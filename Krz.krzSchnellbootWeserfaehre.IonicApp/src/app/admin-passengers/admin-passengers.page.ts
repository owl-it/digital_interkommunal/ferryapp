import { Component, OnInit } from '@angular/core';
import { FerrymenService } from '../services/ferrymen.service';
import { Passengers } from '../models/ferryPassengers.model';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { Toaster } from '../helpers/toaster';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-admin-passengers',
  templateUrl: './admin-passengers.page.html',
  styleUrls: ['./admin-passengers.page.scss'],
})
export class AdminPassengersPage implements OnInit {
  results: Observable<any>;
  passengers: Passengers;
  ferryWorkItemId: string;
  toast: Toaster;

  constructor(private ferrymenService: FerrymenService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private toastController: ToastController) {
                this.toast = new Toaster(toastController);
              }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.passengers = new Passengers();
      this.ferryWorkItemId = this.router.getCurrentNavigation().extras.state.id;
      this.passengers.ferryWorkItemId = this.ferryWorkItemId;
    });
    this.getPassengers();
  }

  getPassengers() {
    this.results = this.ferrymenService.getPassengerCount(this.ferryWorkItemId);
    this.results.pipe(
      catchError(err => {
        console.log('Could not load Passenger Data', err);
        this.toast.presentToast('Passagierdaten konnten nicht geladen werden!', 'error');
        return throwError(new Error('Passagierdaten konnten nicht geladen werden!'));
    }))
    .subscribe(res => {
      this.passengers = res;
    });
  }

  setPassengers() {
    this.results = this.ferrymenService.setPassengerCount(this.passengers);

    this.results.pipe(
      catchError(err => {
        console.log('Could not save Passenger Data', err);
        this.toast.presentToast('Passagierdaten konnten nicht gespeichert werden!', 'error');
        return throwError(new Error('Passagierdaten konnten nicht gespeichert werden!'));
    }))
    .subscribe(res => {
      //this.passengers = res;
      this.router.navigate(['/admin']);
    }, () => this.toast.presentToast('Passagierdaten gespeichert!', 'info')
    );
    // this.results.pipe(
    //   map(res => {
    //     this.passengers = res;
    //   })
    // ).subscribe();
  }
}
