import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminPassengersPage } from './admin-passengers.page';

const routes: Routes = [
  {
    path: '',
    component: AdminPassengersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminPassengersPageRoutingModule {}
