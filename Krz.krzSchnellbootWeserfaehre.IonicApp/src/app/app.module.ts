import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import {LOCALE_ID} from '@angular/core';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './LoginComponent/login.component';
import { PositionComponent } from './popups/position/position.component';
import { FormsModule } from '@angular/forms';
import { IonicStorageModule } from '@ionic/storage-angular';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from './helpers/jwtInterceptor';
import { appInitializer } from './helpers/app.initializer';
import { AccountService } from './services/accounts.service';

@NgModule({
  declarations: [AppComponent, LoginComponent, PositionComponent],
  entryComponents: [],
  imports: [BrowserModule,
            IonicModule.forRoot(),
            IonicStorageModule.forRoot(),
            AppRoutingModule,
            FormsModule,
            HttpClientModule,
            ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production,
            // Register the ServiceWorker as soon as the application is stable
            // or after 30 seconds (whichever comes first).
            registrationStrategy: 'registerWhenStable:30000'
})],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }, {provide: LOCALE_ID, useValue: 'de-DE'},
              { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }],
              //{ provide: APP_INITIALIZER, useFactory: appInitializer, multi: true, deps: [AccountService] },],
  bootstrap: [AppComponent],
})
export class AppModule {}

