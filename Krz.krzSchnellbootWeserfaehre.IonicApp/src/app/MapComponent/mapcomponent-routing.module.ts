import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MapcomponentPage } from './mapcomponent.page';

const routes: Routes = [
  {
    path: '',
    component: MapcomponentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MapcomponentPageRoutingModule {}
