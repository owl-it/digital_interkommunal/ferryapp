import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MapcomponentPageRoutingModule } from './mapcomponent-routing.module';

import { MapcomponentPage } from './mapcomponent.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MapcomponentPageRoutingModule
  ],
  declarations: [MapcomponentPage]
})
export class MapcomponentPageModule {}
