import { Component, OnInit, OnDestroy, ViewContainerRef } from '@angular/core';
import * as Leaflet from 'leaflet';
import { PositionServiceService } from '../services/position-service.service';
import { PositionSensor } from '../models/position-sensor';
import { PositionComponent } from '../popups/position/position.component';
import { Observable, Subject } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { PopupItem } from '../helpers/popup.item';

@Component({
  selector: 'app-mapcomponent',
  templateUrl: './mapcomponent.page.html',
  styleUrls: ['./mapcomponent.page.scss'],
})
export class MapcomponentPage implements OnInit, OnDestroy {
  map: Leaflet.Map;
  positionPopup: Leaflet.Popup;
  position: PositionSensor;
  positionData: Observable<PositionSensor>;
  ferryItem: PopupItem[];

  constructor( private positionservice: PositionServiceService,
               private viewContainerRef: ViewContainerRef,
             ) { }

  ngOnInit() {
    this.retrievePositionData();
    //this.womoItem = this.womoService.getPopups();
    this.map = Leaflet.map('mapId').setView([this.position.lat, this.position.lon], 20);
    Leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
    }).addTo(this.map);
    this.positionPopup = Leaflet.popup({className: 'womoPopup'}).setContent(this.loadPopup());
  }

  ionViewDidEnter() {
    this.leafletMap();
    this.map.invalidateSize();
  }

  leafletMap() {
    const myIcon = Leaflet.icon({iconUrl: 'assets/icons/16.png'});

    Leaflet.marker([
      this.position.lat, this.position.lon],
      {icon: myIcon})
      .bindPopup(this.positionPopup)
      .addTo(this.map)
      .openPopup();
  }

  retrievePositionData() {
    //this.positionData = new Observable<PositionSensor>();
    this.positionData = this.positionservice.getPositionData();

    this.positionData.pipe(
      map(res => {
        this.position = new PositionSensor();
        this.position.battery = res[0].batV;
        this.position.received = res[0].receivedAt;
        this.position.lat = res[0].latitudeDeg;
        this.position.lon = res[0].longitudeDeg;
        this.position.speed = res[0].speedKmph;
        this.position.temperature = res[0].tempC;
        this.ferryItem[0].data = this.position;
      })
    ).subscribe();

  /*   this.positionData.subscribe(res => {
      this.position = res[0];
    }); */
    // this.positionservice.getPositionData().subscribe(res => {
    //   console.log(res);
    //   this.position = new PositionSensor();
    //   this.position.battery = res[0].batV;
    //   this.position.received = res[0].receivedAt;
    //   this.position.lat = res[0].latitudeDeg;
    //   this.position.lon = res[0].longitudeDeg;
    //   this.position.speed = res[0].speedKmph;
    //   this.position.temperature = res[0].tempC;
    // });
  }
  /** Remove map when we have multiple map object */
  ngOnDestroy() {
    this.map.remove();

  }

  loadPopup(): any {
    const posItem = this.ferryItem[0];
    //const viewContainerRef = this.womoHost.viewContainerRef;
    //viewContainerRef.clear();

    const componentRef = this.viewContainerRef.createComponent<PositionComponent>(posItem.component);
    componentRef.instance.position = posItem.data;

    const div = document.createElement('div');
    div.appendChild(componentRef.location.nativeElement);
    return div;
  }
   /**
    * Builds the referenced component so it can be injected into the
    * leaflet map as popup.
    * Original code from:  https://github.com/darkguy2008/leaflet-angular4-issue/blob/master/src/app.ts
    */
  // private compilePopup(component, onAttach): any {
  //   const compFactory: any = this.resolver.resolveComponentFactory(component);
  //   const compRef: any = compFactory.create(this.injector);

  //   // onAttach allows you to assign
  //   if (onAttach) {
  //     onAttach(compRef);
  //   }

  //   this.appRef.attachView(compRef.hostView);
  //   compRef.onDestroy(() => this.appRef.detachView(compRef.hostView));

  //   const div = document.createElement('div');
  //   div.appendChild(compRef.location.nativeElement);
  //   return div;
  // }

}
