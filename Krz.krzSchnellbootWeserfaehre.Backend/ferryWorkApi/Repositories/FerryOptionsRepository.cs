using ferryWorkItems.Contracts;
using ferryWorkItems.Entities;
using ferryWorkItems.Models; 
using Microsoft.EntityFrameworkCore;

namespace ferryWorkItems.Repository
{
    public class FerryOptionsRepository : RepositoryBase<ferryOptions>, IFerryOptionsRepository
    {
        public FerryOptionsRepository(ferryWorkItemContext repositoryContext)
            :base(repositoryContext)
        {
        }
        public async Task<IEnumerable<ferryOptions>> GetAllFerryOptionsAsync()
        {
            return await Task.FromResult(FindAll()
                .OrderBy(u => u.Id)
                .ToList());
        }

        public async Task<ferryOptions> GetFerryOptionsByIdAsync(Guid Id)
        {
            return await Task.FromResult(FindByCondition(u => u.Id.Equals(Id))
                .FirstOrDefault());
        }

        public void CreateFerryOptions(ferryOptions options)
        {
            Create(options);
        }

        public void UpdateFerryOptions(ferryOptions options)
        {
            Update(options);
        }

        public void DeleteFerryOptions(ferryOptions options)
        {
            Delete(options);
        }
    }
}