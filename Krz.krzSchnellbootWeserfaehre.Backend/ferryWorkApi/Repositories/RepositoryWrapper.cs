using ferryWorkItems.Contracts;
using ferryWorkItems.Entities;
using ferryWorkItems.Models;

namespace ferryWorkItems.Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private ferryWorkItemContext _repoContext;
        private IUserRepository _user;
        private IFerryWorkItemRepository _ferryWorkItem;
        private IFerryPassengersRepository _passengers;
        private IFerryOptionsRepository _options;
        public IUserRepository User {
            get {
                if(_user == null)
                {
                    _user = new UserRepository(_repoContext);
                }
                return _user;
            }
        }
        public IFerryWorkItemRepository FerryWorkItem {
            get {
                if(_ferryWorkItem == null)
                {
                    _ferryWorkItem = new FerryWorkItemRepository(_repoContext);
                }
                return _ferryWorkItem;
            }
        }

         public IFerryPassengersRepository FerryPassengers {
            get {
                if(_passengers == null)
                {
                    _passengers = new FerryPassengersRepository(_repoContext);
                }
                return _passengers;
            }
        }
        public IFerryOptionsRepository FerryOptions {
            get {
                if(_options == null)
                {
                    _options = new FerryOptionsRepository(_repoContext);
                }
                return _options;
            }
        }

        public RepositoryWrapper(ferryWorkItemContext repositoryContext)
        {
            _repoContext = repositoryContext;
        }
        public async Task SaveAsync()
        {
            await _repoContext.SaveChangesAsync();
        }
    }
}