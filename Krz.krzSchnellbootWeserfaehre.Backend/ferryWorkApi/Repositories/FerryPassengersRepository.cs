using ferryWorkItems.Contracts;
using ferryWorkItems.Entities;
using ferryWorkItems.Models; 
using Microsoft.EntityFrameworkCore;

namespace ferryWorkItems.Repository
{
    public class FerryPassengersRepository : RepositoryBase<ferryPassengers>, IFerryPassengersRepository
    {
        public FerryPassengersRepository(ferryWorkItemContext repositoryContext)
            :base(repositoryContext)
        {
        }
        public async Task<IEnumerable<ferryPassengers>> GetAllFerryPassengersAsync()
        {
            return await Task.FromResult(FindAll()
                .OrderBy(u => u.Id)
                .ToList());
        }

        public async Task<ferryPassengers> GetFerryPassengersByferryWorkitemIdAsync(Guid Id)
        {
            return await Task.FromResult(FindByCondition(u => u.ferryWorkItemId.Equals(Id))
                .FirstOrDefault());
        }

        public async Task<ferryPassengers> GetFerryPassengersByIdAsync(Guid Id)
        {
            return await Task.FromResult(FindByCondition(u => u.Id.Equals(Id))
                .FirstOrDefault());
        }

        public void CreateFerryPassengers(ferryPassengers passengers)
        {
            Create(passengers);
        }

        public void UpdateFerryPassengers(ferryPassengers passengers)
        {
            Update(passengers);
        }

        public void DeleteFerryPassengers(ferryPassengers passengers)
        {
            Delete(passengers);
        }
    }
}