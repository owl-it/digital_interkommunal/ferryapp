using ferryWorkItems.Contracts;
using ferryWorkItems.Entities;
using ferryWorkItems.Models;
using Microsoft.EntityFrameworkCore;


namespace ferryWorkItems.Repository
{
    public class FerryWorkItemRepository : RepositoryBase<ferryWorkItem>, IFerryWorkItemRepository
    {
        public FerryWorkItemRepository(ferryWorkItemContext repositoryContext)
            :base(repositoryContext)
        {
        }

        public async Task<IEnumerable<ferryWorkItem>> FerryWorkItemsByUserAsync(Guid userId)
        {
            return await Task.FromResult(FindByCondition(a => a.UserId.Equals(userId)).ToList());
        }

         public async Task<IEnumerable<ferryWorkItem>> GetAllFerryWorkItemsAsync()
        {
            return await Task.FromResult(FindByCondition(f => f.start.Year == System.DateTime.Now.Year)
                .OrderBy(f => f.start)
                .Include(userItem => userItem.user)
                .ToList());
        }

        public async Task<IEnumerable<ferryWorkItem>> GetNextFerryWorkItemsAsync()
        {
            DateTime dtNow  = new DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, System.DateTime.Now.Day, 00, 00, 00 );
            // returns the next 4 Operating Days
            return await Task.FromResult(FindByCondition(f => f.start >= dtNow && f.start <= dtNow.AddDays(14))
                .OrderBy(f => f.start )
                .Take(4)
                .ToList());
        }

        public async Task<ferryWorkItem> GetFerryWorkItemByIdAsync(Guid Id)
        {
            return await Task.FromResult(FindByCondition(f => f.Id.Equals(Id))
                .Include(userItem => userItem.user)
                .FirstOrDefault());
        }

        public void InsertFerryOperatingDays()
        {
            
        }
        public void CreateFerryWorkItem(ferryWorkItem item)
        {
            Create(item);
        }

        public void UpdateFerryWorkItem(ferryWorkItem item)
        {
            Update(item);
        }

        public void DeleteFerryWorkItem(ferryWorkItem item)
        {
            Delete(item);
        }
        
    }
}