using ferryWorkItems.Contracts;
using ferryWorkItems.Entities;
using ferryWorkItems.Models; 
using Microsoft.EntityFrameworkCore;

namespace ferryWorkItems.Repository
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(ferryWorkItemContext repositoryContext)
            :base(repositoryContext)
        {
        }
        public async Task<IEnumerable<User>> GetAllUsersAsync()
        {
            return await FindAll()
                .OrderBy(u => u.Id)
                //.Include(fwItem => fwItem.ferryWorkItemDetails)
                .ToListAsync();
        }

         public async Task<IEnumerable<User>> GetAllUsersNoItemsAsync()
        {
            return await FindAll()
                .OrderBy(u => u.Id)
                //.Include(fwItem => fwItem.ferryWorkItemDetails)
                .ToListAsync();
        }
        public async Task<User> GetUserByIdAsync(Guid Id)
        {
            return await Task.FromResult(FindByCondition(u => u.Id.Equals(Id))
                .FirstOrDefault());
        }

        public async Task<User> GetUserWithDetailsAsync(Guid Id)
        {
            return await FindByCondition(u => u.Id.Equals(Id))
                //.Include(fwItem => fwItem.ferryWorkItemDetails)
                .FirstOrDefaultAsync();
        }

        public void CreateUser(User user)
        {
            Create(user);
        }

        public void UpdateUser(User user)
        {
            Update(user);
        }

        public void DeleteUser(User user)
        {
            Delete(user);
        }
    }
}