using ferryWorkItems.Models;
using ferryWorkItems.Models.Users;
using ferryWorkItems.Entities;


namespace AutoMapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ferryWorkItem, ferryWorkItemDTO>();

            CreateMap<User, UserDTO>();

            CreateMap<UserDTO, User>();

            CreateMap<CreateUserDTO, User>();

            CreateMap<UpdateUserDTO, User>();

            CreateMap<CreateFerryWorkItemDTO, ferryWorkItem>();

            CreateMap<UpdateFerryWorkItemDTO, ferryWorkItem>();
            
            CreateMap<CreateFerryPassengersDTO, ferryPassengers>();

            CreateMap<UpdateFerryPassengersDTO, ferryPassengers>();

            CreateMap<ferryPassengers, ferrypassengersDTO>();

             CreateMap<CreateFerryOptionsDTO, ferryOptions>();

            CreateMap<UpdateFerryOptionsDTO, ferryOptions>();

            CreateMap<ferryOptions, ferryOptionsDTO>();

        }
    }
}