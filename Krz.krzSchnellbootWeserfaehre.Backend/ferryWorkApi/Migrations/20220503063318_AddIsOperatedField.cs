﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ferryWorkApi.Migrations
{
    public partial class AddIsOperatedField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsOperating",
                table: "ferryWorkItems",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsOperating",
                table: "ferryWorkItems");
        }
    }
}
