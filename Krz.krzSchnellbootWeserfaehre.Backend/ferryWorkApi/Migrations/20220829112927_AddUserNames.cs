﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ferryWorkApi.Migrations
{
    public partial class AddUserNames : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Nachname",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Vorname",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Nachname",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Vorname",
                table: "Users");
        }
    }
}
