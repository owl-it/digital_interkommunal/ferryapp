﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ferryWorkApi.Migrations
{
    public partial class AddPassengerTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ferryPassengers",
                columns: table => new
                {
                    ferryPassengersId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    pedestrians = table.Column<int>(type: "int", nullable: false),
                    cyclists = table.Column<int>(type: "int", nullable: false),
                    wheelchairs = table.Column<int>(type: "int", nullable: false),
                    ferryWorkItemId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ferryPassengers", x => x.ferryPassengersId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ferryPassengers");
        }
    }
}
