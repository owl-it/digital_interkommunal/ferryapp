﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ferryWorkApi.Migrations
{
    public partial class AddedFerryWorkOptions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ferryWorkOptions",
                columns: table => new
                {
                    ferryWorkOptionId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    seasonStartDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    seasonEndDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    operatingStartTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    operatingEndTime = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ferryWorkOptions", x => x.ferryWorkOptionId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ferryWorkOptions");
        }
    }
}
