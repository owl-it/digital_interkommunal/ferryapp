﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ferryWorkApi.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "ferryWorkItems",
                columns: table => new
                {
                    ferryWorkItemId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    workDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    start = table.Column<DateTime>(type: "datetime2", nullable: false),
                    end = table.Column<DateTime>(type: "datetime2", nullable: false),
                    comment = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ferryWorkItems", x => x.ferryWorkItemId);
                    table.ForeignKey(
                        name: "FK_ferryWorkItems_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ferryWorkItems_UserId",
                table: "ferryWorkItems",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ferryWorkItems");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
