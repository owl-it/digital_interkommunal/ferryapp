// // Bedienen der Google Business Profile API
// using ferryWorkItems.Models;
// using ferryWorkItems.Contracts;
// using AutoMapper;
// using System;
// using System.Net;
// using System.Net.Http;
// using System.Net.Http.Headers;
// using System.Threading.Tasks;
// using Microsoft.AspNetCore.Mvc;
// using Google.Apis.Auth.OAuth2;
// using Google.Apis.MyBusinessAccountManagement.v1;
// using Google.Apis.Services;
// using Newtonsoft.Json;
// using System;
// using System.IO;
// using System.Threading;

// namespace ferryWorkItems.Services
// {
//     public class ListAccounts
//     {
//         public class WebCredentials
//         {
//             public class Web
//             {
//                 public string client_id { set; get; }
//                 public string client_secret { set; get; }
//             }
//             public Web web { get; set; }

//         }

//         public void getGmbData()        
//         {
//             string path = Path.Combine(Directory.GetCurrentDirectory(), "client_secrets.json");
//             System.Diagnostics.Debug.WriteLine("Reading credentials from " + path);
//             string contents = File.ReadAllText(path);
//             var deserializedCredentials = JsonConvert.DeserializeObject<WebCredentials>(contents);

//             var ClientId = deserializedCredentials.web.client_id;
//             var ClientSecret = deserializedCredentials.web.client_secret;
//             Console.WriteLine("Obtained client id " + ClientId + " and secret " + ClientSecret);
//             string[] scopes = new string[] { "https://www.googleapis.com/auth/plus.business.manage" };

//             var userCredential = GoogleWebAuthorizationBroker.AuthorizeAsync(
//                 new ClientSecrets
//                 {
//                     ClientId = ClientId,
//                     ClientSecret = ClientSecret,
//                 },
//                 scopes,
//                 "user",
//                 CancellationToken.None).Result;

//             var service = new MyBusinessAccountManagementService(new BaseClientService.Initializer() { HttpClientInitializer = userCredential });

//             var accountsListResponse = service.Accounts.List().Execute();
//             Console.WriteLine(JsonConvert.SerializeObject(accountsListResponse));
//             var name = Console.ReadLine();

//         }

//     }

// }