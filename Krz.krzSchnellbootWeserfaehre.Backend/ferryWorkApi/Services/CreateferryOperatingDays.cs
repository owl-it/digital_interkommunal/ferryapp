using ferryWorkItems.Models;

namespace ferryWorkItems.Services
{
    public class CreateFerryOperatingDays
    {
        public List<DateTime> getDays(List<DateTime> dtOpDays)
        {
            Dictionary<int,int> days = new Dictionary<int, int>();
            days.Add(4,30);
            days.Add(5,31);
            days.Add(6,30);
            days.Add(7,31);
            days.Add(8,31);
            days.Add(9,30);
            days.Add(10,31);

            foreach(KeyValuePair<int,int> keys in days)
            {
                for(int day = 1; day<=keys.Value; day++)
                {
                    DateTime dtOperatingDay = new DateTime(System.DateTime.Now.Year, keys.Key, day);
                    if (dtOperatingDay.DayOfWeek == DayOfWeek.Sunday || dtOperatingDay.DayOfWeek == DayOfWeek.Saturday)
                    {
                        // post new Operatingday towards the ferryWorkItems
                        dtOpDays.Add(dtOperatingDay);
                    }
                }
            }
            return dtOpDays;
        }

        public List<DateTime> getHolidays()
        {
            int year = System.DateTime.Now.Year;

            List<DateTime> dtHolidays = new List<DateTime>();
            
            // Add the fixed holidays to the Array
            // Tag der Arbeit 01.05
            dtHolidays.Add(new DateTime(year, 5, 1));
            // Tag der deutschen Einheit 03.10
            dtHolidays.Add(new DateTime(year, 10, 3));

            // Calculate Easterdate for the given year and set the bankholidays depending on that day
            DateTime easterDate = Easter(year);
            // Eastersunday
            dtHolidays.Add(easterDate);
            // Good Friday - 2 days before easter sunday
            dtHolidays.Add(easterDate.AddDays(-2));
            // Easter monday - 1 day after easter sunday
            dtHolidays.Add(easterDate.AddDays(1));
            // Ascension of Christ - 39 days after easter sunday
            dtHolidays.Add(easterDate.AddDays(39));
            // Whit monday - 50 days after easter sunday
            dtHolidays.Add(easterDate.AddDays(50));
            // corpus christi - 60 days after easter sunday
            dtHolidays.Add(easterDate.AddDays(60));

            return dtHolidays;
        }

        private static DateTime Easter(int year) {
            int a = year%19;
            int b = year/100;
            int c = (b - (b/4) - ((8*b + 13)/25) + (19*a) + 15)%30;
            int d = c - (c/28)*(1 - (c/28)*(29/(c + 1))*((21 - a)/11));
            int e = d - ((year + (year/4) + d + 2 - b + (b/4))%7);
            int month = 3 + ((e + 40)/44);
            int day = e + 28 - (31*(month/4));
            return new DateTime(year, month , day);
        }   

    }
}