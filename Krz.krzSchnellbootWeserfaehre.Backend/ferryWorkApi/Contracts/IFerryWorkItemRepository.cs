using ferryWorkItems.Contracts;
using ferryWorkItems.Entities;

namespace ferryWorkItems.Models
{
    public interface IFerryWorkItemRepository : IRepositoryBase<ferryWorkItem>
    {
        Task<IEnumerable<ferryWorkItem>> FerryWorkItemsByUserAsync(Guid userId);
        Task<IEnumerable<ferryWorkItem>> GetAllFerryWorkItemsAsync();
        Task<ferryWorkItem> GetFerryWorkItemByIdAsync(Guid ferryWorkItemId);
        Task<IEnumerable<ferryWorkItem>> GetNextFerryWorkItemsAsync();
        void InsertFerryOperatingDays();
        void CreateFerryWorkItem(ferryWorkItem item);
        void UpdateFerryWorkItem(ferryWorkItem item);
        void DeleteFerryWorkItem(ferryWorkItem item);
    }
}
