using ferryWorkItems.Models;

namespace ferryWorkItems.Contracts
{
    public interface IRepositoryWrapper
    {
        IUserRepository User { get; }
        IFerryWorkItemRepository FerryWorkItem { get; }
        IFerryPassengersRepository FerryPassengers { get; }
        IFerryOptionsRepository FerryOptions { get; }
        Task SaveAsync();
    }
}