using ferryWorkItems.Contracts;
using ferryWorkItems.Entities;

namespace ferryWorkItems.Models
{
    public interface IUserRepository : IRepositoryBase<User>
    {
        Task<IEnumerable<User>> GetAllUsersAsync();
        Task<IEnumerable<User>> GetAllUsersNoItemsAsync();
        Task<User> GetUserByIdAsync(Guid userId);
        Task<User> GetUserWithDetailsAsync(Guid userId);
        void CreateUser(User user);
        void UpdateUser(User user);
        void DeleteUser(User user);
    }
}
