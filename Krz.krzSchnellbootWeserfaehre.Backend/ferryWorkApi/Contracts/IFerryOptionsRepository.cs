using ferryWorkItems.Contracts;
using ferryWorkItems.Entities;

namespace ferryWorkItems.Models
{
    public interface IFerryOptionsRepository : IRepositoryBase<ferryOptions>
    {
        Task<IEnumerable<ferryOptions>> GetAllFerryOptionsAsync();
        Task<ferryOptions> GetFerryOptionsByIdAsync(Guid ferryOptionsId);
        void CreateFerryOptions(ferryOptions options);
        void UpdateFerryOptions(ferryOptions options);
        void DeleteFerryOptions(ferryOptions options);
    }
}
