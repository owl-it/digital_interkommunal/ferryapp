using ferryWorkItems.Contracts;
using ferryWorkItems.Entities;

namespace ferryWorkItems.Models
{
    public interface IFerryPassengersRepository : IRepositoryBase<ferryPassengers>
    {
        Task<IEnumerable<ferryPassengers>> GetAllFerryPassengersAsync();
        Task<ferryPassengers> GetFerryPassengersByIdAsync(Guid ferryPassengersId);
        Task<ferryPassengers> GetFerryPassengersByferryWorkitemIdAsync(Guid ferryWorkItemId);
        void CreateFerryPassengers(ferryPassengers passengers);
        void UpdateFerryPassengers(ferryPassengers passengers);
        void DeleteFerryPassengers(ferryPassengers passengers);
    }
}
