using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ferryWorkItems.Entities
{
    [Table("ferryPassengers")]
    public class ferryPassengers
    {
        [Required]
        [Column("ferryPassengersId")]
        public Guid Id { get; set; }
        public int pedestrians { get; set; }
        public int cyclists { get; set; }
        public int wheelchairs { get; set; }
        public Guid ferryWorkItemId { get; set; }
    }
}
