using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ferryWorkItems.Entities
{
    [Table("ferryWorkOptions")]
    public class ferryOptions
    {
        [Required]
        [Column("ferryWorkOptionId")]
        public Guid Id { get; set; }
        [Required]
        public DateTime seasonStartDate { get; set; }
        [Required]
        public DateTime seasonEndDate { get; set; }
        [Required]
        public DateTime operatingStartTime { get; set; }
        [Required]
        public DateTime operatingEndTime { get; set; }
    }
}
