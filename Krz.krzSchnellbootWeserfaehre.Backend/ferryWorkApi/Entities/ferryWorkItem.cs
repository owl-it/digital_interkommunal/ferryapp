﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ferryWorkItems.Entities
{
    [Table("ferryWorkItems")]
    public class ferryWorkItem
    {
        [Required]
        [Column("ferryWorkItemId")]
        public Guid Id { get; set; }
        [Required]
        public DateTime workDate { get; set; }
        [Required]
        public DateTime start { get; set; }
        [Required]
        public DateTime end { get; set; }
        public string? comment { get; set; }
        public bool IsOperating { get; set; }

        [Required]
        [ForeignKey(nameof(User))]
        public Guid UserId { get; set; }
        public User user { get; set; }
    }
}
