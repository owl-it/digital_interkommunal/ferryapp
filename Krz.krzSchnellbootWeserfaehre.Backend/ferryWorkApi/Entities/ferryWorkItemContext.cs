﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;
using ferryWorkItems.Entities;

namespace ferryWorkItems.Models
{
    public class ferryWorkItemContext : DbContext
    {
        public DbSet<ferryWorkItem> ferryWorkItems { get; set; } = null!;
        public DbSet<User> Users {get; set;} = null!;
        public DbSet<ferryPassengers> FerryPassengers {get; set;} = null!;
        public DbSet<ferryOptions> FerryOptions {get; set;} = null!;
        
        private readonly IConfiguration Configuration;

        public ferryWorkItemContext(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            // connect to sqlite database
            options.UseSqlServer(Configuration.GetConnectionString("WebApiDatabase"));
        }
    }
}