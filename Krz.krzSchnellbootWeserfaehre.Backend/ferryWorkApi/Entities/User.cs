﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ferryWorkItems.Entities
{
    [Table("Users")]
    public class User
    {
        [Required]
        [Column("UserId")]
        public Guid Id { get; set; }
        [Required]
        public string? Email { get; set; }
        public string? Vorname { get; set; }
        public string? Nachname { get; set; }
        //public IEnumerable<ferryWorkItem>? ferryWorkItemDetails { get; set; }
    }
}
