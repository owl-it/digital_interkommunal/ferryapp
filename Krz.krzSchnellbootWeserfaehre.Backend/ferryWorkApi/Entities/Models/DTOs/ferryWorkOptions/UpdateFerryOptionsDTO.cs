namespace ferryWorkItems.Models
{ 
    public class UpdateFerryOptionsDTO {
        public DateTime seasonStartDate { get; set; }
        public DateTime seasonEndDate { get; set; }
        public DateTime operatingStartTime { get; set; }
        public DateTime operatingEndTime { get; set; }
    }
}