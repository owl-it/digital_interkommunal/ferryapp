using ferryWorkItems.Models.Users;

namespace ferryWorkItems.Models
{
    public class CreateFerryWorkItemDTO
    {
        public DateTime workDate { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public string? comment { get; set; }
        public bool IsOperating { get; set; }
        public Guid UserId { get; set; }
    }
}