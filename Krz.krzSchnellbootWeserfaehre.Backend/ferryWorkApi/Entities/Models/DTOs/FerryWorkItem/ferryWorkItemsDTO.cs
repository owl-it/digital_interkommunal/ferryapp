using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ferryWorkItems.Models.Users;

namespace ferryWorkItems.Models
{
    public class ferryWorkItemDTO
    {
        public Guid Id { get; set; }
        public DateTime workDate { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public string? comment { get; set; }
        public bool IsOperating { get; set; }
        public Guid userId { get; set; }
        public UserDTO? user { get; set; }
    }
}
