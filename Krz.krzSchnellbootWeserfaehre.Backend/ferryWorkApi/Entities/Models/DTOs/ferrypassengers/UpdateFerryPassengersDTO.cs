public class UpdateFerryPassengersDTO {
    public int pedestrians { get; set; }
    public int cyclists { get; set; }
    public int wheelchairs { get; set; }
}