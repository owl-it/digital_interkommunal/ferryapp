public class CreateFerryPassengersDTO {
    public int pedestrians { get; set; }
    public int cyclists { get; set; }
    public int wheelchairs { get; set; }
    public Guid ferryWorkItemId { get; set; }
}