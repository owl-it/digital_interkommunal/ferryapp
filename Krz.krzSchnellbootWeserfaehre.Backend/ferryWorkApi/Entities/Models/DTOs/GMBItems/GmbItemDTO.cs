using Google.Apis.MyBusinessAccountManagement.v1;

namespace ferryWorkItems.Models
{
    public class GmbItemDTO
    {
        public DateOnly startDate { get; set; }
        public TimeOnly openTime { get; set; }
        public DateOnly endDate { get; set; }
        public TimeOnly closeTime { get; set; }
        public bool closed { get; set; }
    }
}