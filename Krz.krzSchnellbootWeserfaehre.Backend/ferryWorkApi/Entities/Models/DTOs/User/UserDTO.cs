using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ferryWorkItems.Models.Users
{
    public class UserDTO
    {
        public Guid Id { get; set; }
        public string? Email { get; set; }
         public string? Vorname { get; set; }
        public string? Nachname { get; set; }
        //public IEnumerable<ferryWorkItemDTO>? ferryWorkItemDetails { get; set; }
    }
}
