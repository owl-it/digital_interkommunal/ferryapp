#nullable disable
using Microsoft.AspNetCore.Mvc;
using ferryWorkItems.Models;
using ferryWorkItems.Models.Users;
using ferryWorkItems.Contracts;
using ferryWorkItems.Entities;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace ferryWorkItems.Controllers
{
    [Authorize]
    [Route("api/ferrypassengers")]
    [ApiController]
    public class ferrypassengersController : ControllerBase
    {
        
        private IRepositoryWrapper _repository;
        private IMapper _mapper;

        private readonly ferryWorkItemContext _context;

        public ferrypassengersController(ferryWorkItemContext context, IRepositoryWrapper repository, IMapper mapper)
        {
            _context = context;
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllFerryPassengersAsync()
        {
            try
            {
                var passengers = await _repository.FerryPassengers.GetAllFerryPassengersAsync();
               
                var mapper = InitializeAutoMapper();
                var passengersDTOs = mapper.Map<IEnumerable<ferrypassengersDTO>>(passengers);
                return Ok(passengersDTOs); 
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }

        [HttpGet("{id}", Name="FerryPassengersById") ]
        public async Task<IActionResult> GetFerryPassengersById(Guid id)
        {
            try
            {
                var passengers = await _repository.FerryPassengers.GetFerryPassengersByIdAsync(id);
                if (passengers is null)
                {
                    return NotFound();
                }
                else
                {
                    // add FerryWorkItems to User.
                    var result = _mapper.Map<ferrypassengersDTO>(passengers);
                    //var fwItems = await _repository.FerryWorkItem.FerryWorkItemsByUserAsync(id);
                    //result.ferryWorkItemDetails = _mapper.Map<IEnumerable<ferryWorkItemDTO>>(fwItems);
                    return Ok(result); 
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }

        [HttpGet("ferrypassengersbyitem/{id}", Name="PassengersByFerryWorkitemId") ]
        public async Task<IActionResult> GetFerryPassengersByFerryWorkItemId(Guid id)
        {
            try
            {
                var passengers = await _repository.FerryPassengers.GetFerryPassengersByferryWorkitemIdAsync(id);
                if (passengers is null)
                {
                    return NotFound();
                }
                else
                {
                    // add FerryWorkItems to User.
                    var result = _mapper.Map<ferrypassengersDTO>(passengers);
                    //var fwItems = await _repository.FerryWorkItem.FerryWorkItemsByUserAsync(id);
                    //result.ferryWorkItemDetails = _mapper.Map<IEnumerable<ferryWorkItemDTO>>(fwItems);
                    return Ok(result); 
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateFerryPassengers([FromBody]CreateFerryPassengersDTO passengers)
        {
            try
            {
                if (passengers is null)
                {
                }
                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }
                var passengersEntity = _mapper.Map<ferryPassengers>(passengers);
                _repository.FerryPassengers.CreateFerryPassengers(passengersEntity);
                await _repository.SaveAsync();
                var createdFerryPassengers = _mapper.Map<ferrypassengersDTO>(passengersEntity);
                return CreatedAtRoute("FerryPassengersById", new { id = createdFerryPassengers.Id }, createdFerryPassengers);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message + " " + ex.InnerException);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateFerryPassengers(Guid id, [FromBody]UpdateFerryPassengersDTO passengers)
        {
            try
            {
                if (passengers is null)
                {
                    return BadRequest("passengers object is null");
                }
                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }
                var passengersEntity = await _repository.FerryPassengers.GetFerryPassengersByIdAsync(id);
                if (passengersEntity is null)
                {
                    return NotFound();
                }
                _mapper.Map(passengers, passengersEntity);
                _repository.FerryPassengers.UpdateFerryPassengers(passengersEntity);
                await _repository.SaveAsync();
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }
        
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFerryPassengers(Guid id)
        {
            try
            {
                var passengers = await _repository.FerryPassengers.GetFerryPassengersByIdAsync(id);
                if(passengers == null)
                {
                    return NotFound();
                }

                _repository.FerryPassengers.DeleteFerryPassengers(passengers);
                await _repository.SaveAsync();
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }

        private static Mapper InitializeAutoMapper()
        {
            var config = new MapperConfiguration(cfg=> {
                cfg.CreateMap<ferryPassengers, ferrypassengersDTO>();
                //cfg.CreateMap<User, UserDTO>();
            });

            var mapper = new Mapper(config);
            return mapper;
        }
    }
}
