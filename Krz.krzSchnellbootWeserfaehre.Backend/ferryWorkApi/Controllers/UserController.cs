#nullable disable
using Microsoft.AspNetCore.Mvc;
using ferryWorkItems.Models;
using ferryWorkItems.Models.Users;
using ferryWorkItems.Contracts;
using ferryWorkItems.Entities;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace ferryWorkItems.Controllers
{
    [Authorize]
    [Route("api/user")]
    [ApiController]
    public class userController : ControllerBase
    {
        
        private IRepositoryWrapper _repository;
        private IMapper _mapper;

        private readonly ferryWorkItemContext _context;

        public userController(ferryWorkItemContext context, IRepositoryWrapper repository, IMapper mapper)
        {
            _context = context;
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllUsersAsync()
        {
            try
            {
                var users = await _repository.User.GetAllUsersAsync();
               
                var mapper = InitializeAutoMapper();
                var userDTOs = mapper.Map<IEnumerable<UserDTO>>(users);
                return Ok(userDTOs); 
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }

        [Route("/api/usersplain")]
        [HttpGet]
        public async Task<IActionResult> GetAllUsersNoItemsAsync()
        {
            try
            {
                var users = await _repository.User.GetAllUsersNoItemsAsync();
               
                var mapper = InitializeAutoMapper();
                var userDTOs = mapper.Map<IEnumerable<UserDTO>>(users);
                return Ok(userDTOs); 
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }

        [HttpGet("{id}", Name="UserById") ]
        public async Task<IActionResult> GetUserById(Guid id)
        {
            try
            {
                var user = await _repository.User.GetUserByIdAsync(id);
                if (user is null)
                {
                    return NotFound();
                }
                else
                {
                    // add FerryWorkItems to User.
                    var result = _mapper.Map<UserDTO>(user);
                    //var fwItems = await _repository.FerryWorkItem.FerryWorkItemsByUserAsync(id);
                    //result.ferryWorkItemDetails = _mapper.Map<IEnumerable<ferryWorkItemDTO>>(fwItems);
                    return Ok(result); 
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }

        [HttpGet("{id}/ferryWorkItem")]
        public async Task<IActionResult> GetUserWithDetails(Guid id)
        {
            try
            {
                var user = await _repository.User.GetUserWithDetailsAsync(id);
                if (user == null)
                {
                    return NotFound();
                }
                else
                {
                    var result = _mapper.Map<UserDTO>(user);
                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateUser([FromBody]CreateUserDTO user)
        {
            try
            {
                if (user is null)
                {
                }
                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }
                var userEntity = _mapper.Map<User>(user);
                _repository.User.CreateUser(userEntity);
                await _repository.SaveAsync();
                var createdUser = _mapper.Map<UserDTO>(userEntity);
                return CreatedAtRoute("UserById", new { id = createdUser.Id }, createdUser);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message + " " + ex.InnerException);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateUser(Guid id, [FromBody]UpdateUserDTO user)
        {
            try
            {
                if (user is null)
                {
                    return BadRequest("user object is null");
                }
                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }
                var userEntity = await _repository.User.GetUserByIdAsync(id);
                if (userEntity is null)
                {
                    return NotFound();
                }
                _mapper.Map(user, userEntity);
                _repository.User.UpdateUser(userEntity);
                await _repository.SaveAsync();
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }
        
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(Guid id)
        {
            try
            {
                var user = await _repository.User.GetUserByIdAsync(id);
                if(user == null)
                {
                    return NotFound();
                }
                var fwItems = await _repository.FerryWorkItem.FerryWorkItemsByUserAsync(id);
                if(fwItems.Any())
                {
                    return BadRequest("Cannot delete User. It has related FerryWorkItems. Delete those Items first");
                }

                _repository.User.DeleteUser(user);
                await _repository.SaveAsync();
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }

        private static Mapper InitializeAutoMapper()
        {
            var config = new MapperConfiguration(cfg=> {
                cfg.CreateMap<ferryWorkItem, ferryWorkItemDTO>();
                cfg.CreateMap<User, UserDTO>();
            });

            var mapper = new Mapper(config);
            return mapper;
        }
    }
}
