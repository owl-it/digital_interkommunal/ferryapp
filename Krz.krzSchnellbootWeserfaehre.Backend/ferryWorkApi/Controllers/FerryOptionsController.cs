#nullable disable
using Microsoft.AspNetCore.Mvc;
using ferryWorkItems.Models;
using ferryWorkItems.Models.Users;
using ferryWorkItems.Contracts;
using ferryWorkItems.Entities;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace ferryWorkItems.Controllers
{
    [Authorize]
    [Route("api/ferryoptions")]
    [ApiController]
    public class ferryOptionsController : ControllerBase
    {
        
        private IRepositoryWrapper _repository;
        private IMapper _mapper;

        private readonly ferryWorkItemContext _context;

        public ferryOptionsController(ferryWorkItemContext context, IRepositoryWrapper repository, IMapper mapper)
        {
            _context = context;
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllFerryOptionsAsync()
        {
            try
            {
                var options = await _repository.FerryOptions.GetAllFerryOptionsAsync();
               
                var mapper = InitializeAutoMapper();
                var optionsDTOs = mapper.Map<IEnumerable<ferryOptionsDTO>>(options);
                return Ok(optionsDTOs); 
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }

        [HttpGet("{id}", Name="FerryOptionsById") ]
        public async Task<IActionResult> GetFerryOptionsById(Guid id)
        {
            try
            {
                var options = await _repository.FerryOptions.GetFerryOptionsByIdAsync(id);
                if (options is null)
                {
                    return NotFound();
                }
                else
                {
                    // add FerryWorkItems to User.
                    var result = _mapper.Map<ferryOptionsDTO>(options);
                    //var fwItems = await _repository.FerryWorkItem.FerryWorkItemsByUserAsync(id);
                    //result.ferryWorkItemDetails = _mapper.Map<IEnumerable<ferryWorkItemDTO>>(fwItems);
                    return Ok(result); 
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateFerryOptions([FromBody]CreateFerryOptionsDTO options)
        {
            try
            {
                if (options is null)
                {
                }
                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }
                var optionsEntity = _mapper.Map<ferryOptions>(options);
                _repository.FerryOptions.CreateFerryOptions(optionsEntity);
                await _repository.SaveAsync();
                var createdFerryOptions = _mapper.Map<ferryOptionsDTO>(optionsEntity);
                return CreatedAtRoute("FerryOptionsById", new { id = createdFerryOptions.Id }, createdFerryOptions);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message + " " + ex.InnerException);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateFerryOptions(Guid id, [FromBody]UpdateFerryOptionsDTO options)
        {
            try
            {
                if (options is null)
                {
                    return BadRequest("options object is null");
                }
                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }
                var optionsEntity = await _repository.FerryOptions.GetFerryOptionsByIdAsync(id);
                if (optionsEntity is null)
                {
                    return NotFound();
                }
                _mapper.Map(options, optionsEntity);
                _repository.FerryOptions.UpdateFerryOptions(optionsEntity);
                await _repository.SaveAsync();
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }
        
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFerryOptions(Guid id)
        {
            try
            {
                var options = await _repository.FerryOptions.GetFerryOptionsByIdAsync(id);
                if(options == null)
                {
                    return NotFound();
                }

                _repository.FerryOptions.DeleteFerryOptions(options);
                await _repository.SaveAsync();
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }

        private static Mapper InitializeAutoMapper()
        {
            var config = new MapperConfiguration(cfg=> {
                cfg.CreateMap<ferryOptions, ferryOptionsDTO>();
                //cfg.CreateMap<User, UserDTO>();
            });

            var mapper = new Mapper(config);
            return mapper;
        }
    }
}
