#nullable disable
using Microsoft.AspNetCore.Mvc;
using ferryWorkItems.Models;
using ferryWorkItems.Services;
using ferryWorkItems.Contracts;
using ferryWorkItems.Entities;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace ferryWorkItems.Controllers
{
    [Authorize]
    [Route("api/ferryworkitem")]
    [ApiController]
    public class ferryWorkItemController : ControllerBase
    {
        
        private IRepositoryWrapper _repository;
        private IMapper _mapper;

        private readonly ferryWorkItemContext _context;

        public ferryWorkItemController(
            ferryWorkItemContext context,
            IRepositoryWrapper repository,
            IMapper mapper)
        {
            _context = context;
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetAllFerryWorkItems()
        {
            try
            {
                var items = _repository.FerryWorkItem.GetAllFerryWorkItemsAsync();

                var result = _mapper.Map<IEnumerable<ferryWorkItemDTO>>(items.Result);
                return Ok(result); 
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }
        
        [AllowAnonymous]
        [HttpGet("upcoming", Name="NextFerryWorkItems")]
        public IActionResult GetNextFerryWorkItems()
        {
            try
            {
                var items = _repository.FerryWorkItem.GetNextFerryWorkItemsAsync();
                
                // Test: get Google Business Account-List
                // ListAccounts lstGmb = new ListAccounts();
                // lstGmb.getGmbData();
                var result = _mapper.Map<IEnumerable<ferryWorkItemDTO>>(items.Result);
                return Ok(result); 
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }

        [HttpGet("{id}", Name="FerryWorkItemById") ]
        public async Task<IActionResult> GetFerryWorkItemById(Guid id)
        {
            try
            {
                var item = await _repository.FerryWorkItem.GetFerryWorkItemByIdAsync(id);
                if (item is null)
                {
                    return NotFound();
                }
                else
                {
                    var result = _mapper.Map<ferryWorkItemDTO>(item);
                    return Ok(result); 
                }
            }
            catch (Exception ex)
            {
                    return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateFerryWorkItem([FromBody]CreateFerryWorkItemDTO item)
        {
            try
            {
                if (item is null)
                {
                }
                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }

                // Create a new user if the current user id is not exisiting.
                // check if the id already exists
                var user = _repository.User.GetUserByIdAsync(item.UserId);
                if(user.Result is null)
                {
                    // then create a new user
                    _repository.User.CreateUser(new User{ Id = item.UserId, Email = "" });
                }
                var itemEntity = _mapper.Map<ferryWorkItem>(item);
                _repository.FerryWorkItem.CreateFerryWorkItem(itemEntity);
                await _repository.SaveAsync();
                var createdItem = _mapper.Map<ferryWorkItemDTO>(itemEntity);
                return CreatedAtRoute("FerryWorkItemById", new { id = createdItem.Id }, createdItem);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }

        [HttpPost("insertferryoperatingdays", Name="InsertFerryOperatingDays")]
        public async Task<IActionResult> InsertFerryOperatingDays()
        {
            try
            {
                CreateFerryWorkItemDTO item;
                CreateFerryOperatingDays cod = new CreateFerryOperatingDays();
                List<DateTime> dtOpDays = cod.getHolidays();
                List<DateTime> dtDays = cod.getDays(dtOpDays);
                //var user = _repository.User.GetUserByIdAsync(id);
                var options = await _repository.FerryOptions.GetAllFerryOptionsAsync();
                var option = options.FirstOrDefault();
               
                foreach(DateTime dtDate in dtDays) {
                    item = new CreateFerryWorkItemDTO();
                    item.workDate = dtDate;
                    item.start = new DateTime(dtDate.Year, dtDate.Month, dtDate.Day, option.operatingStartTime.Hour, option.operatingStartTime.Minute, 0);
                    item.end = new DateTime(dtDate.Year, dtDate.Month, dtDate.Day, option.operatingEndTime.Hour, option.operatingEndTime.Minute, 0);
                    item.comment = "";
                    item.IsOperating = true;
                    //item.UserId = user.Result.Id;
                    var itemEntity = _mapper.Map<ferryWorkItem>(item);
                    _repository.FerryWorkItem.CreateFerryWorkItem(itemEntity);
                    await _repository.SaveAsync();
                }

                return StatusCode(201, "FerryOperatingDays sucessfully created!");
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateFerryWorkItem(Guid id, [FromBody]UpdateFerryWorkItemDTO item)
        {
            try
            {
                if (item is null)
                {
                    return BadRequest("FerryWorkItem object is null");
                }
                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }
                var itemEntity = await _repository.FerryWorkItem.GetFerryWorkItemByIdAsync(id);
                if (itemEntity is null)
                {
                    return NotFound();
                }
                _mapper.Map(item, itemEntity);
                _repository.FerryWorkItem.UpdateFerryWorkItem(itemEntity);
                await _repository.SaveAsync();
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }
        
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFerryWorkItem(Guid id)
        {
            try
            {
                var item = await _repository.FerryWorkItem.GetFerryWorkItemByIdAsync(id);
                if(item == null)
                {
                    return NotFound();
                }
                _repository.FerryWorkItem.DeleteFerryWorkItem(item);
                await _repository.SaveAsync();
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }        
    }
}
