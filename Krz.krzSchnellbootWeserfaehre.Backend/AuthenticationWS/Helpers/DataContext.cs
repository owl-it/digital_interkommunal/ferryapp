namespace WebApi.Helpers;

using Microsoft.EntityFrameworkCore;
using WebApi.Entities;
using WebApi.Entities.Configuration;

public class DataContext : DbContext
{
    public DbSet<Account> Accounts { get; set; }
    //public DbSet<RefreshToken> RefreshTokens {get; set; }

    private readonly IConfiguration Configuration;

    public DataContext(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    protected override void OnConfiguring(DbContextOptionsBuilder options)
    {
        // connect to sqlite database
        options.UseSqlServer(Configuration.GetConnectionString("WebApiDatabase"));
    }

    //protected override void OnModelCreating(ModelBuilder modelBuilder)
    //{
        //modelBuilder.ApplyConfiguration(new AccountConfiguration());
        //modelBuilder.ApplyConfiguration(new RefreshTokenConfiguration());

        //  modelBuilder.Entity<Account>().HasData(
        //     new Account()
        //         {
        //             Id = new Guid("54B38580-8A22-4F28-91B0-08D9FC2C27EE"),
        //             Title = "Frau",
        //             FirstName = "Test",
        //             LastName = "Test",
        //             Email = "d.bahl@krz.de",
        //             PasswordHash = "$2a$11$nQs2GO6G7g80q8VSat94qOuPeRBRaWBJZnmfTYdFdjMIGSKllyJrq",
        //             AcceptTerms = true,
        //             Role = Role.Admin,
        //             VerificationToken = "384A6A0DC5CCCDC93433771F0C43242832DA7EFCEE982389B8C4FA694B3789D3BA9D3CBC308ADB6BE2E9AE3227A9252B7E0035A37E002C23ED6D786AEFD58737",
        //             Verified = DateTime.Now,
        //             ResetToken = null,
        //             ResetTokenExpires = null,
        //             PasswordReset = null,
        //             Created = DateTime.Now,
        //             Updated = null
        //         } 
        //  );
        // modelBuilder.Entity<RefreshToken>().HasData(
        //     new RefreshToken()
        //     {
        //         Id = new Guid("6963B669-FE43-4393-537F-08D9FC2F00EB"),
        //         AccountId = new Guid("6963B669-FE43-4393-537F-08D9FC2F00EB"),
        //         Token = "491F3FCDB8DF532FD8A43A7260783DBB101E1BCB7CE9AC6B6307144F33268CF437D23C59CC1135449E7089615A6C784EEC72870B03AB844939A810B2566DD4A5",
        //         Expires = Convert.ToDateTime("2022-03-09 09:28:25.8485616"),
        //         Created = Convert.ToDateTime("2022-03-02 09:28:25.8485973"),
        //         CreatedByIp = "0.0.0.1",
        //         Revoked = Convert.ToDateTime("2022-03-02 09:33:24.2473156"),
        //         RevokedByIp = "0.0.0.1",
        //         ReplacedByToken = "9EC1B73B424D7D582096736A6F545A56B9DE8977017DF02CCB9110730A389647A9261B088C13D78B998EFBFD4DF6F9CFE77491339FF24D994BA327EA3B80DF3B",
        //         ReasonRevoked = "Replaced by new token"
        //     } 
        // );
    //}
}