namespace WebApi.Entities.Configuration;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebApi.Entities;
using WebApi.Services;

public class RefreshTokenConfiguration : IEntityTypeConfiguration<RefreshToken>
{
    public void Configure(EntityTypeBuilder<RefreshToken> builder)
    {
        builder.ToTable("RefreshToken");
        builder.Property(RefreshToken => RefreshToken.Id)
            .IsRequired(true);
        builder.Property(RefreshToken => RefreshToken.AccountId)
            .IsRequired(true);
        builder.Property(RefreshToken => RefreshToken.Token);
        builder.Property(RefreshToken => RefreshToken.Expires)
            .IsRequired(true);
        builder.Property(RefreshToken => RefreshToken.Created)
            .IsRequired(true);
        builder.Property(RefreshToken => RefreshToken.CreatedByIp);
        builder.Property(RefreshToken => RefreshToken.Revoked);
        builder.Property(RefreshToken => RefreshToken.RevokedByIp);
        builder.Property(RefreshToken => RefreshToken.ReplacedByToken);
        builder.Property(RefreshToken => RefreshToken.ReasonRevoked);

        // builder.HasOne(RefreshToken => RefreshToken.Account)
        //     .WithMany(RefreshToken => RefreshToken.RefreshTokens)
        //     .HasForeignKey("AccountId");

        builder.HasData
        (
            new RefreshToken()
            {
                Id = new Guid("6963B669-FE43-4393-537F-08D9FC2F00EB"),
                AccountId = new Guid("6963B669-FE43-4393-537F-08D9FC2F00EB"),
                Token = "491F3FCDB8DF532FD8A43A7260783DBB101E1BCB7CE9AC6B6307144F33268CF437D23C59CC1135449E7089615A6C784EEC72870B03AB844939A810B2566DD4A5",
                Expires = Convert.ToDateTime("2022-03-09 09:28:25.8485616"),
                Created = Convert.ToDateTime("2022-03-02 09:28:25.8485973"),
                CreatedByIp = "0.0.0.1",
                Revoked = Convert.ToDateTime("2022-03-02 09:33:24.2473156"),
                RevokedByIp = "0.0.0.1",
                ReplacedByToken = "9EC1B73B424D7D582096736A6F545A56B9DE8977017DF02CCB9110730A389647A9261B088C13D78B998EFBFD4DF6F9CFE77491339FF24D994BA327EA3B80DF3B",
                ReasonRevoked = "Replaced by new token"
            } 
        );

        
            
    }
}