namespace WebApi.Entities.Configuration;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebApi.Entities;
using WebApi.Entities.Configuration;

public class AccountConfiguration : IEntityTypeConfiguration<Account>
{
    public void Configure(EntityTypeBuilder<Account> builder)
    {
        builder.ToTable("Account");
        builder.Property(account => account.Id)
            .IsRequired(true);
        builder.Property(account => account.Title);
        builder.Property(account => account.FirstName);
        builder.Property(account => account.LastName);
        builder.Property(account => account.Email);
        builder.Property(account => account.PasswordHash);
        builder.Property(account => account.AcceptTerms)
            .IsRequired(true);
        builder.Property(account => account.Role)
            .IsRequired(true);
        builder.Property(account => account.VerificationToken);
        builder.Property(account => account.Verified);
        builder.Property(account => account.ResetToken);
        builder.Property(account => account.ResetTokenExpires);
        builder.Property(account => account.PasswordReset);
        builder.Property(account => account.Created)
            .IsRequired(true);
        builder.Property(account => account.Updated);
        
        builder.HasData
        (
            new Account()
            {
                Id = new Guid("54B38580-8A22-4F28-91B0-08D9FC2C27EE"),
                Title = "Frau",
                FirstName = "Test",
                LastName = "Test",
                Email = "d.bahl@krz.de",
                PasswordHash = "$2a$11$nQs2GO6G7g80q8VSat94qOuPeRBRaWBJZnmfTYdFdjMIGSKllyJrq",
                AcceptTerms = true,
                Role = Role.Admin,
                VerificationToken = "384A6A0DC5CCCDC93433771F0C43242832DA7EFCEE982389B8C4FA694B3789D3BA9D3CBC308ADB6BE2E9AE3227A9252B7E0035A37E002C23ED6D786AEFD58737",
                Verified = DateTime.Now,
                ResetToken = null,
                ResetTokenExpires = null,
                PasswordReset = null,
                Created = DateTime.Now,
                Updated = null
            } 
        );

    
            
    }
}