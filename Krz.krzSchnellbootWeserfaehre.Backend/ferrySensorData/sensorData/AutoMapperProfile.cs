using sensorData.Models;

namespace AutoMapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<VWindsensor, VWindsensorDTO>();
            CreateMap<VPegelsensor, VPegelsensorDTO>();
            CreateMap<VGpsSensor, VGpssensorDTO>();
        }
    }
}