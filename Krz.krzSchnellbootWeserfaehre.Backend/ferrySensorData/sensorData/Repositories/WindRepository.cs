using sensorData.Models; 
using Microsoft.EntityFrameworkCore;

namespace sensorData.Repository
{
    public class WindRepository : RepositoryBase<VWindsensor>, IWindRepository
    {
        public WindRepository(sensorContext repositoryContext)
            :base(repositoryContext)
        {
        }

        public async Task<IEnumerable<VWindsensor>> GetLastWindDataAsync()
        {
            return await FindAll()
                .ToListAsync();
        }
    }
}