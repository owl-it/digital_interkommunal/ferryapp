using sensorData.Contracts;
using sensorData.Models;

namespace sensorData.Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private sensorContext _repoContext;
        private IWindRepository _wind;
        private IPegelRepository _pegel;
        private IGpsRepository _gps;
        public IWindRepository VWindsensor {
            get {
                if(_wind == null)
                {
                    _wind = new WindRepository(_repoContext);
                }
                return _wind;
            }
        }
        public IPegelRepository VPegelsensor {
            get {
                if(_pegel == null)
                {
                    _pegel = new PegelRepository(_repoContext);
                }
                return _pegel;
            }
        }

        public IGpsRepository VGpsSensor {
            get {
                if(_gps == null)
                {
                    _gps = new GpsRepository(_repoContext);
                }
                return _gps;
            }
        }

        public RepositoryWrapper(sensorContext repositoryContext)
        {
            _repoContext = repositoryContext;
        }
        public async Task SaveAsync()
        {
            await _repoContext.SaveChangesAsync();
        }
    }
}