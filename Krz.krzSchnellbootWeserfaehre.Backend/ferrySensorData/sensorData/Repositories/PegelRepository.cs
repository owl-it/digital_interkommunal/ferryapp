using sensorData.Models; 
using Microsoft.EntityFrameworkCore;

namespace sensorData.Repository
{
    public class PegelRepository : RepositoryBase<VPegelsensor>, IPegelRepository
    {
        public PegelRepository(sensorContext repositoryContext)
            :base(repositoryContext)
        {
        }

        public async Task<IEnumerable<VPegelsensor>> GetLastPegelDataAsync()
        {
            return await FindAll()
                .ToListAsync();
        }
    }
}