using sensorData.Models; 
using Microsoft.EntityFrameworkCore;

namespace sensorData.Repository
{
    public class GpsRepository : RepositoryBase<VGpsSensor>, IGpsRepository
    {
        public GpsRepository(sensorContext repositoryContext)
            :base(repositoryContext)
        {
        }

        public async Task<IEnumerable<VGpsSensor>> GetLastPositionDataAsync()
        {
            return await FindAll()
                .ToListAsync();
        }
    }
}