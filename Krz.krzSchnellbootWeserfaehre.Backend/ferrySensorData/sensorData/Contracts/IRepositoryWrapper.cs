using sensorData.Models;

namespace sensorData.Contracts
{
    public interface IRepositoryWrapper
    {
        IWindRepository VWindsensor { get; }
        IPegelRepository VPegelsensor { get; }
        IGpsRepository VGpsSensor { get; }
        Task SaveAsync();
    }
}