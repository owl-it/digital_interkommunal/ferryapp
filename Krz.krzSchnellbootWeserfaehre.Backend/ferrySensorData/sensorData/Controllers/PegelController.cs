#nullable disable
using Microsoft.AspNetCore.Mvc;
using sensorData.Models;
using sensorData.Contracts;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace sensorData.Controllers
{
    [Authorize]
    [Route("api/pegel")]
    [ApiController]
    public class pegelController : ControllerBase
    {
        private IRepositoryWrapper _repository;
        private IMapper _mapper;
        private readonly sensorContext _context;

        public pegelController(sensorContext context, IRepositoryWrapper repository, IMapper mapper)
        {
            _context = context;
            _repository = repository;
            _mapper = mapper;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetLastPegelDataAsync()
        {
            try
            {
                var pegelData = await _repository.VPegelsensor.GetLastPegelDataAsync();
                
                if(pegelData == null)
                {
                    return NotFound();
                }
                else
                {
                    var result = _mapper.Map<IEnumerable<VPegelsensorDTO>>(pegelData);
                    return Ok(result); 
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }
    }
}
