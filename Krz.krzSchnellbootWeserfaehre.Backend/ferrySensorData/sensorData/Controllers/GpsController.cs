#nullable disable
using Microsoft.AspNetCore.Mvc;
using sensorData.Models;
using sensorData.Contracts;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace sensorData.Controllers
{
    [Authorize]
    [Route("api/position")]
    [ApiController]
    public class gpsController : ControllerBase
    {
        private IRepositoryWrapper _repository;
        private IMapper _mapper;
        private readonly sensorContext _context;

        public gpsController(sensorContext context, IRepositoryWrapper repository, IMapper mapper)
        {
            _context = context;
            _repository = repository;
            _mapper = mapper;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetLastPositionDataAsync()
        {
            try
            {
                var gpsData = await _repository.VGpsSensor.GetLastPositionDataAsync();
                
                if(gpsData == null)
                {
                    return NotFound();
                }
                else
                {
                    var result = _mapper.Map<IEnumerable<VGpssensorDTO>>(gpsData);
                    return Ok(result); 
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }
    }
}
