#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using sensorData.Models;
using sensorData.Contracts;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace sensorData.Controllers
{
    [Authorize]
    [Route("api/wind")]
    [ApiController]
    public class windController : ControllerBase
    {
        
        private IRepositoryWrapper _repository;
        private IMapper _mapper;

        private readonly sensorContext _context;

        public windController(sensorContext context, IRepositoryWrapper repository, IMapper mapper)
        {
            _context = context;
            _repository = repository;
            _mapper = mapper;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetLastWindDataAsync()
        {
            try
            {
                var windData = await _repository.VWindsensor.GetLastWindDataAsync();

                if(windData == null)
                {
                    return NotFound();
                }
                else
                {
                    var result = _mapper.Map<IEnumerable<VWindsensorDTO>>(windData);
                    return Ok(result); 
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }
    }
}
