
namespace sensorData.Models
{
    public partial class VPegelsensorDTO
    {
        public int Id { get; set; }
        public DateTime? ReceivedAt { get; set; }
        public decimal? _1Battery { get; set; }
        public short? _2Distance { get; set; }
    }
}
