﻿
namespace sensorData.Models
{
    public partial class VGpssensorDTO
    {
        public int Id { get; set; }
        public DateTime? ReceivedAt { get; set; }
        public string? Type { get; set; }
        public decimal? BatV { get; set; }
        public decimal? GpsAccM { get; set; }
        public decimal? HeadingDeg { get; set; }
        public double? LatitudeDeg { get; set; }
        public double? LongitudeDeg { get; set; }
        public decimal? SpeedKmph { get; set; }
        public decimal? TempC { get; set; }
        public decimal? VAin { get; set; }
        public decimal? VExt { get; set; }
    }
}
