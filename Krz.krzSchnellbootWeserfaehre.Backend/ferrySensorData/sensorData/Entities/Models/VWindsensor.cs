﻿
namespace sensorData.Models
{
    public partial class VWindsensor
    {
        public int Id { get; set; }
        public DateTime? ReceivedAt { get; set; }
        public string? _1Type { get; set; }
        public decimal? _2Battery { get; set; }
        public string? _3WindAve10 { get; set; }
        public string? _4WindMax10 { get; set; }
        public string? _5WindMin10 { get; set; }
        public string? _6DirAve10 { get; set; }
        public string? _7DirMax10 { get; set; }
        public string? _8DirHi10 { get; set; }
        public string? _9DirLo10 { get; set; }
    }
}
