using sensorData.Contracts;

namespace sensorData.Models
{
    public interface IGpsRepository : IRepositoryBase<VGpsSensor>
    {
        Task<IEnumerable<VGpsSensor>> GetLastPositionDataAsync();
    }
}
