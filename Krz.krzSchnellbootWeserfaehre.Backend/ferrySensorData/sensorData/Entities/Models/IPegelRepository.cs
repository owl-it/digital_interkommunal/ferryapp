using sensorData.Contracts;

namespace sensorData.Models
{
    public interface IPegelRepository : IRepositoryBase<VPegelsensor>
    {
        Task<IEnumerable<VPegelsensor>> GetLastPegelDataAsync();
    }
}
