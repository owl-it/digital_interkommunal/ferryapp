using sensorData.Contracts;

namespace sensorData.Models
{
    public interface IWindRepository : IRepositoryBase<VWindsensor>
    {
        Task<IEnumerable<VWindsensor>> GetLastWindDataAsync();
    }
}
