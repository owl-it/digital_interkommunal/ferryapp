﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace sensorData.Models
{
    public partial class sensorContext : DbContext
    {
        private readonly IConfiguration Configuration;
        public virtual DbSet<VWindsensor> VWindsensors { get; set; } = null!;
        public virtual DbSet<VPegelsensor> VPegelsensors { get; set; } = null!;
        public virtual DbSet<VGpsSensor> VGpssensors { get; set; } = null!;

        public sensorContext(DbContextOptions<sensorContext> options, IConfiguration configuration)
            : base(options)
        {
            Configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(Configuration.GetConnectionString("WebApiDatabase"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<VGpsSensor>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("v_gpsSensor");

                entity.Property(e => e.BatV)
                    .HasColumnType("numeric(6, 3)")
                    .HasColumnName("batV");

                entity.Property(e => e.GpsAccM)
                    .HasColumnType("numeric(3, 0)")
                    .HasColumnName("gpsAccM");

                entity.Property(e => e.HeadingDeg)
                    .HasColumnType("numeric(3, 0)")
                    .HasColumnName("headingDeg");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(e => e.LatitudeDeg).HasColumnName("latitudeDeg");

                entity.Property(e => e.LongitudeDeg).HasColumnName("longitudeDeg");

                entity.Property(e => e.MaxTempC)
                    .HasColumnType("numeric(4, 1)")
                    .HasColumnName("maxTempC");

                entity.Property(e => e.MinTempC)
                    .HasColumnType("numeric(4, 1)")
                    .HasColumnName("minTempC");

                entity.Property(e => e.ReceivedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("received_at");

                entity.Property(e => e.SpeedKmph)
                    .HasColumnType("numeric(3, 0)")
                    .HasColumnName("speedKmph");

                entity.Property(e => e.TempC)
                    .HasColumnType("numeric(4, 1)")
                    .HasColumnName("tempC");

                entity.Property(e => e.Type)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("_type");

                entity.Property(e => e.VAin)
                    .HasColumnType("numeric(6, 3)")
                    .HasColumnName("vAin");

                entity.Property(e => e.VExt)
                    .HasColumnType("numeric(6, 3)")
                    .HasColumnName("vExt");
            });

            modelBuilder.Entity<VPegelsensor>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("v_pegelsensor");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(e => e.ReceivedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("received_at");

                entity.Property(e => e._1Battery)
                    .HasColumnType("numeric(3, 1)")
                    .HasColumnName("1_Battery");

                entity.Property(e => e._2Distance).HasColumnName("2_Distance");
            });

            modelBuilder.Entity<VWindsensor>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("v_windsensor");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(e => e.ReceivedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("received_at");

                entity.Property(e => e._1Type)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("1_Type");

                entity.Property(e => e._2Battery)
                    .HasColumnType("numeric(3, 1)")
                    .HasColumnName("2_Battery");

                entity.Property(e => e._3WindAve10)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("3_Wind_ave10");

                entity.Property(e => e._4WindMax10)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("4_Wind_max10");

                entity.Property(e => e._5WindMin10)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("5_Wind_min10");

                entity.Property(e => e._6DirAve10)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("6_Dir_ave10");

                entity.Property(e => e._7DirMax10)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("7_Dir_max10");

                entity.Property(e => e._8DirHi10)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("8_Dir_hi10");

                entity.Property(e => e._9DirLo10)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("9_Dir_lo10");
            });

            
            OnModelCreatingPartial(modelBuilder);
        }
        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
